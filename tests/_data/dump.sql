-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.25-0ubuntu0.16.04.2-log - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table upspot.assistants
CREATE TABLE IF NOT EXISTS `assistants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table upspot.assistants: ~1 rows (approximately)
/*!40000 ALTER TABLE `assistants` DISABLE KEYS */;
INSERT INTO `assistants` (`id`, `user_id`, `proposal_id`) VALUES
	(1, 1, 2);
/*!40000 ALTER TABLE `assistants` ENABLE KEYS */;

-- Dumping structure for table upspot.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table upspot.categories: ~6 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `slug`, `parent_id`) VALUES
	(1, 'Sport', 'sport', NULL),
	(2, 'Travel', 'travel', NULL),
	(3, 'Education', 'education', NULL),
	(5, 'Volleyball', 'volleyball', 1),
	(6, 'Football', 'football', 1),
	(7, 'English Learning', 'english-learning', 3);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table upspot.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposal_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table upspot.comments: ~1 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`id`, `proposal_id`, `user_id`, `text`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, 'Some text', '2019-02-14 12:26:27', '2019-02-14 12:26:28');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping structure for table upspot.doctrine_migrations
CREATE TABLE IF NOT EXISTS `doctrine_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table upspot.doctrine_migrations: ~22 rows (approximately)
/*!40000 ALTER TABLE `doctrine_migrations` DISABLE KEYS */;
INSERT INTO `doctrine_migrations` (`version`, `executed_at`) VALUES
	('20190205130648', '2019-02-05 14:00:36'),
	('20190210140205', '2019-02-10 14:04:51'),
	('20190210142526', '2019-02-10 14:31:11'),
	('20190210153408', '2019-02-10 15:38:59'),
	('20190210154349', '2019-02-10 15:55:11'),
	('20190211104845', '2019-02-11 10:53:38'),
	('20190211160943', '2019-02-11 16:27:57'),
	('20190211163811', '2019-02-11 16:42:47'),
	('20190212143926', '2019-02-12 14:46:16'),
	('20190212145504', '2019-02-12 14:56:36'),
	('20190213094401', '2019-02-13 09:49:28'),
	('20190213094931', '2019-02-13 09:52:24'),
	('20190213132921', '2019-02-13 13:33:20'),
	('20190213133913', '2019-02-13 13:41:08'),
	('20190215075954', '2019-02-15 08:01:42'),
	('20190215080152', '2019-02-15 08:12:59'),
	('20190215080825', '2019-02-15 08:12:59'),
	('20190215081305', '2019-02-15 08:17:02'),
	('20190215092559', '2019-02-15 09:26:52'),
	('20190220112114', '2019-02-20 14:24:07'),
	('20190220112419', '2019-02-20 14:27:52'),
	('20190220114101', '2019-02-20 11:41:43');
/*!40000 ALTER TABLE `doctrine_migrations` ENABLE KEYS */;

-- Dumping structure for table upspot.proposals
CREATE TABLE IF NOT EXISTS `proposals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `cost` decimal(8,2) DEFAULT NULL,
  `assistants_count` int(11) NOT NULL DEFAULT '0',
  `max_people_count` int(11) DEFAULT NULL,
  `max_girls_count` int(11) DEFAULT NULL,
  `max_boys_count` int(11) DEFAULT NULL,
  `comments_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table upspot.proposals: ~2 rows (approximately)
/*!40000 ALTER TABLE `proposals` DISABLE KEYS */;
INSERT INTO `proposals` (`id`, `user_id`, `title`, `short_description`, `description`, `location`, `date`, `date_start`, `date_end`, `cost`, `assistants_count`, `max_people_count`, `max_girls_count`, `max_boys_count`, `comments_count`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Lorem Lipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Moscow, Russia', NULL, '2019-02-10', '2019-02-15', NULL, 0, 4, NULL, NULL, 0, '2019-02-10 19:11:20', '2019-02-20 14:34:21'),
	(2, 2, 'Quo cu perfecto sensibus', 'Usu in nobis doming graeco, cum no evertitur contentiones.', 'Usu in nobis doming graeco, cum no evertitur contentiones. Quo cu perfecto sensibus intellegebat, ex quo natum everti consequuntur. An meis adolescens has, detraxit atomorum intellegebat sit no. Ex noster abhorreant voluptatum eum, te mei epicurei abhorreant sententiae, latine habemus vis eu. Nam eu dico electram, soleat mollis in nam, intellegat quaerendum vel id. Munere cetero voluptatibus no per, vis te sumo quando.', 'Saint-Petersburg, Russia', NULL, '2019-03-15', '2019-03-25', NULL, 1, 4, NULL, NULL, 1, '2019-02-11 19:11:20', '2019-02-20 14:34:40');
/*!40000 ALTER TABLE `proposals` ENABLE KEYS */;

-- Dumping structure for table upspot.proposals_to_categories
CREATE TABLE IF NOT EXISTS `proposals_to_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposal_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table upspot.proposals_to_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `proposals_to_categories` DISABLE KEYS */;
INSERT INTO `proposals_to_categories` (`id`, `proposal_id`, `category_id`) VALUES
	(1, 1, 5),
	(2, 2, 6);
/*!40000 ALTER TABLE `proposals_to_categories` ENABLE KEYS */;

-- Dumping structure for table upspot.timetable
CREATE TABLE IF NOT EXISTS `timetable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposal_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `people_count` int(11) NOT NULL DEFAULT '0',
  `girls_count` int(11) NOT NULL DEFAULT '0',
  `boys_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table upspot.timetable: ~2 rows (approximately)
/*!40000 ALTER TABLE `timetable` DISABLE KEYS */;
INSERT INTO `timetable` (`id`, `proposal_id`, `date`, `date_start`, `date_end`, `time`, `time_start`, `time_end`, `people_count`, `girls_count`, `boys_count`, `created_at`, `updated_at`) VALUES
	(1, 1, '2019-03-13', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, '2019-02-10 12:26:52', '2019-02-20 16:06:33'),
	(2, 2, NULL, '2019-03-16', '2019-03-17', NULL, NULL, NULL, 0, 0, 0, '2019-02-10 12:26:52', '2019-02-20 14:37:00');
/*!40000 ALTER TABLE `timetable` ENABLE KEYS */;

-- Dumping structure for table upspot.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table upspot.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `login`, `email`, `firstname`, `lastname`, `gender`, `created_at`, `updated_at`) VALUES
	(1, 'johndoe', 'johndoe@gmail.com', 'John', 'Doe', 1, '2019-02-10 18:41:25', '2019-02-20 14:32:29'),
	(2, 'janedoe', 'janedoe@gmail.com', 'Jane', 'Doe', 0, '2019-02-10 18:41:25', '2019-02-20 16:03:51'),
	(3, 'willsmith', 'willsmith@gmail.com', 'Will', 'Smith', 1, '2019-02-20 16:03:44', '2019-02-20 16:05:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table upspot.users_to_timetable
CREATE TABLE IF NOT EXISTS `users_to_timetable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table upspot.users_to_timetable: ~0 rows (approximately)
/*!40000 ALTER TABLE `users_to_timetable` DISABLE KEYS */;
INSERT INTO `users_to_timetable` (`id`, `user_id`, `time_id`, `created_at`) VALUES
	(1, 3, 1, '2019-02-20 16:06:54');
/*!40000 ALTER TABLE `users_to_timetable` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
