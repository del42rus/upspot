<?php 

class UsersCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function getUser(ApiTester $I)
    {
        $I->sendGET('/users/1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'login' => 'string',
            'firstname' => 'string',
            'lastname' => 'string',
            'email' => 'string:email',
            'gender' => 'integer',
            'created_at' => 'string',
            'updated_at' => 'string'
        ]);
    }

    public function getUserCollection(ApiTester $I)
    {
        $I->sendGET('/users');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('$._total_items');
        $I->seeResponseJsonMatchesJsonPath('$._page');
        $I->seeResponseJsonMatchesJsonPath('$._page_count');
        $I->seeResponseJsonMatchesJsonPath('$._embedded.users');
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'login' => 'string',
            'firstname' => 'string',
            'lastname' => 'string',
            'email' => 'string:email',
            'gender' => 'integer',
            'created_at' => 'string',
            'updated_at' => 'string'
        ], '$._embedded.users[*]');
    }
}
