<?php

declare(strict_types=1);

namespace Hal;

use Hal\Metadata\MetadataMapDelegatorFactory;
use Zend\Expressive\Hal\Metadata\MetadataMap;

/**
 * The configuration provider for the Hal module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => [
                'delegators' => [
                    MetadataMap::class => [
                        MetadataMapDelegatorFactory::class
                    ],
                ],
            ],
        ];
    }
}
