<?php

namespace Hal\Metadata;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\DelegatorFactoryInterface;

class MetadataMapDelegatorFactory implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        $metadataMap = new MetadataMapDelegator($callback());

        return $metadataMap;
    }
}