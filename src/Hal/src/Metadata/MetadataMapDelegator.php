<?php

namespace Hal\Metadata;

use Doctrine\ORM\Proxy\Proxy;
use ReflectionClass;
use Zend\Expressive\Hal\Metadata\AbstractMetadata;
use Zend\Expressive\Hal\Metadata\MetadataMap;

class MetadataMapDelegator extends MetadataMap
{
    private $metadataMap;

    public function __construct(MetadataMap $metadataMap)
    {
        $this->metadataMap = $metadataMap;
    }

    public function has(string $class): bool
    {
        $class = $this->getDoctrineEntityClassNameFromProxy($class);

        return $this->metadataMap->has($class);
    }

    public function get(string $class): AbstractMetadata
    {
        $class = $this->getDoctrineEntityClassNameFromProxy($class);

        return $this->metadataMap->get($class);
    }

    private function getDoctrineEntityClassNameFromProxy($class) : string
    {
        $reflectionClass = new ReflectionClass($class);

        if (!$reflectionClass->implementsInterface(Proxy::class)) {
            return $class;
        }

        return $reflectionClass->getParentClass()->getName();
    }
}