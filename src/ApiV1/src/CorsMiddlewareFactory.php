<?php

namespace ApiV1;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Tuupola\Middleware\CorsMiddleware;
use Zend\ProblemDetails\ProblemDetailsResponseFactory;
use Zend\Log;

class CorsMiddlewareFactory
{
    /** @var ProblemDetailsResponseFactory */
    private static $problemDetailsResponseFactory;

    public function __invoke(ContainerInterface $container) : CorsMiddleware
    {
        self::$problemDetailsResponseFactory = $container->get(ProblemDetailsResponseFactory::class);

        $zendLogLogger = new Log\Logger;

        $writer = new Log\Writer\Stream(getcwd() . '/data/log/cors.log');
        $zendLogLogger->addWriter($writer);

        $psrLogger = new Log\PsrLoggerAdapter($zendLogLogger);

        return new CorsMiddleware([
            "origin" => ["*"],
            "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
            "headers.allow" => ["Content-Type", "Accept", "Authorization"],
            "headers.expose" => [],
            "credentials" => false,
            "cache" => 0,
            "error" => [$this, 'error'],
            "logger" => $psrLogger
        ]);
    }

    public static function error(RequestInterface $request, ResponseInterface $response, $arguments)
    {
        return self::$problemDetailsResponseFactory->createResponse(
            $request,
            401,
            '',
            $arguments['message'],
            '',
            []
        );
    }
}