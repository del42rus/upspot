<?php

declare(strict_types=1);

namespace ApiV1;

use App\Domain\Collection\AssistantCollection;
use App\Domain\Collection\CommentParentThreadCollection;
use App\Domain\Collection\FolloweePaginator;
use App\Domain\Collection\FollowerPaginator;
use App\Domain\Collection\ParticipantPaginator;
use App\Domain\DTO\Participant;
use App\Domain\Entity\Category;
use App\Domain\Collection\CategoryCollection;
use App\Domain\Collection\CommentPaginator;
use App\Domain\Collection\ProposalPaginator;
use App\Domain\Collection\TimePaginator;
use App\Domain\Collection\UserPaginator;
use App\Domain\Entity\Comment;
use App\Domain\Entity\Proposal;
use App\Domain\Entity\Time;
use App\Domain\Entity\User;
use Tuupola\Middleware\CorsMiddleware;
use Zend\Expressive\Hal\LinkGenerator\ExpressiveUrlGeneratorFactory;
use Zend\Expressive\Hal\LinkGeneratorFactory;
use Zend\Expressive\Hal\Metadata\MetadataMap;
use Zend\Expressive\Hal\Metadata\RouteBasedCollectionMetadata;
use Zend\Expressive\Hal\Metadata\RouteBasedResourceMetadata;
use Zend\Expressive\Hal\ResourceGeneratorFactory;
use Zend\Expressive\Helper\UrlHelperFactory;
use Zend\Expressive\Helper\UrlHelperMiddlewareFactory;
use Zend\Expressive\Router\FastRouteRouterFactory;
use Zend\Expressive\Router\Middleware\RouteMiddlewareFactory;
use Zend\Hydrator\ArraySerializableHydrator;

use ApiV1\Handler;

class ConfigProvider
{
    public function __invoke() : array
    {
        return [
            'dependencies' => [
                'invokables' => [

                ],
                'factories'  => [
                    Handler\CategoryCollectionShowHandler::class => Handler\Factory\CategoryCollectionShowHandlerFactory::class,
                    Handler\CategoryShowHandler::class => Handler\Factory\CategoryShowHandlerFactory::class,
                    Handler\ProposalCollectionShowHandler::class => Handler\Factory\ProposalCollectionShowHandlerFactory::class,
                    Handler\ProposalShowHandler::class => Handler\Factory\ProposalShowHandlerFactory::class,
                    Handler\ProposalLikeHandler::class => Handler\Factory\ProposalLikeHandlerFactory::class,
                    Handler\ProposalUnlikeHandler::class => Handler\Factory\ProposalUnlikeHandlerFactory::class,
                    Handler\CommentCollectionShowHandler::class => Handler\Factory\CommentCollectionShowHandlerFactory::class,
                    Handler\CommentShowHandler::class => Handler\Factory\CommentShowHandlerFactory::class,
                    Handler\CommentCreateHandler::class => Handler\Factory\CommentCreateHandlerFactory::class,
                    Handler\CommentUpdateHandler::class => Handler\Factory\CommentUpdateHandlerFactory::class,
                    Handler\CommentDeleteHandler::class => Handler\Factory\CommentDeleteHandlerFactory::class,
                    Handler\CommentLikeHandler::class => Handler\Factory\CommentLikeHandlerFactory::class,
                    Handler\CommentUnlikeHandler::class => Handler\Factory\CommentUnlikeHandlerFactory::class,
                    Handler\TimeCollectionShowHandler::class => Handler\Factory\TimeCollectionShowHandlerFactory::class,
                    Handler\TimeShowHandler::class => Handler\Factory\TimeShowHandlerFactory::class,
                    Handler\UserCollectionShowHandler::class => Handler\Factory\UserCollectionShowHandlerFactory::class,
                    Handler\UserShowHandler::class => Handler\Factory\UserShowHandlerFactory::class,
                    Handler\UserUpdateHandler::class => Handler\Factory\UserUpdateHandlerFactory::class,
                    Handler\UserSelfUpdateHandler::class => Handler\Factory\UserSelfUpdateHandlerFactory::class,
                    Handler\PasswordChangeHandler::class => Handler\Factory\PasswordChangeHandlerFactory::class,
                    Handler\ParticipantCollectionShowHandler::class => Handler\Factory\ParticipantCollectionShowHandlerFactory::class,
                    Handler\LoginHandler::class => Handler\Factory\LoginHandlerFactory::class,
                    Handler\RegisterHandler::class => Handler\Factory\RegisterHandlerFactory::class,
                    Handler\ParticipateHandler::class => Handler\Factory\ParticipateHandlerFactory::class,
                    Handler\WithdrawHandler::class => Handler\Factory\WithdrawHandlerFactory::class,
                    Handler\LikedProposalCollectionShowHandler::class => Handler\Factory\LikedProposalCollectionShowHandlerFactory::class,
                    Handler\FollowerCollectionShowHandler::class => Handler\Factory\FollowerCollectionShowHandlerFactory::class,
                    Handler\FolloweeCollectionShowHandler::class => Handler\Factory\FolloweeCollectionShowHandlerFactory::class,
                    Handler\UserFollowHandler::class => Handler\Factory\UserFollowHandlerFactory::class,
                    Handler\UserUnfollowHandler::class => Handler\Factory\UserUnfollowHandlerFactory::class,
                    Handler\UserBlockHandler::class => Handler\Factory\UserBlockHandlerFactory::class,
                    Handler\UserUnblockHandler::class => Handler\Factory\UserUnblockHandlerFactory::class,

                    LinkGenerator::class          => new LinkGeneratorFactory(UrlGenerator::class),
                    ResourceGenerator::class      => new ResourceGeneratorFactory(LinkGenerator::class),
                    Router::class                 => FastRouteRouterFactory::class,
                    RouteMiddleware::class        => new RouteMiddlewareFactory(Router::class),
                    UrlHelper::class              => new UrlHelperFactory('/api', Router::class),
                    UrlHelperMiddleware::class    => new UrlHelperMiddlewareFactory(UrlHelper::class),
                    UrlGenerator::class           => new ExpressiveUrlGeneratorFactory(UrlHelper::class),

                    CorsMiddleware::class => CorsMiddlewareFactory::class,

                    Pipeline::class => PipelineFactory::class,
                ],
            ],
            'hydrators' => [
                'delegators' => [

                ]
            ],
            MetadataMap::class => [
                [
                    '__class__' => RouteBasedResourceMetadata::class,
                    'resource_class' => Category::class,
                    'route' => 'categories.show',
                    'extractor' => ArraySerializableHydrator::class,
                ],
                [
                    '__class__'           => RouteBasedCollectionMetadata::class,
                    'collection_class'    => CategoryCollection::class,
                    'collection_relation' => 'categories',
                    'route'               => 'categories.list',
                ],
                [
                    '__class__' => RouteBasedResourceMetadata::class,
                    'resource_class' => Proposal::class,
                    'route' => 'proposals.show',
                    'extractor' => ArraySerializableHydrator::class,
                ],
                [
                    '__class__'           => RouteBasedCollectionMetadata::class,
                    'collection_class'    => ProposalPaginator::class,
                    'collection_relation' => 'proposals',
                    'route'               => 'proposals.list',
                ],
                [
                    '__class__' => RouteBasedResourceMetadata::class,
                    'resource_class' => User::class,
                    'route' => 'users.show',
                    'extractor' => ArraySerializableHydrator::class,
                ],
                [
                    '__class__'           => RouteBasedCollectionMetadata::class,
                    'collection_class'    => UserPaginator::class,
                    'collection_relation' => 'users',
                    'route'               => 'users.list',
                ],
                [
                    '__class__'           => RouteBasedCollectionMetadata::class,
                    'collection_class'    => AssistantCollection::class,
                    'collection_relation' => 'assistants',
                    'route'               => 'assistants.list',
                ],
                [
                    '__class__' => RouteBasedResourceMetadata::class,
                    'resource_class' => Comment::class,
                    'route' => 'comments.show',
                    'extractor' => ArraySerializableHydrator::class,
                ],
                [
                    '__class__'           => RouteBasedCollectionMetadata::class,
                    'collection_class'    => CommentPaginator::class,
                    'collection_relation' => 'comments',
                    'route'               => 'comments.list',
                ],
                [
                    '__class__'           => RouteBasedCollectionMetadata::class,
                    'collection_class'    => CommentParentThreadCollection::class,
                    'collection_relation' => 'parent_thread',
                    'route'               => 'comments.parent_thread',
                ],
                [
                    '__class__' => RouteBasedResourceMetadata::class,
                    'resource_class' => Time::class,
                    'route' => 'timetable.show',
                    'extractor' => ArraySerializableHydrator::class,
                ],
                [
                    '__class__'           => RouteBasedCollectionMetadata::class,
                    'collection_class'    => TimePaginator::class,
                    'collection_relation' => 'timetable',
                    'route'               => 'timetable.list',
                ],
                [
                    '__class__' => RouteBasedResourceMetadata::class,
                    'resource_class' => Participant::class,
                    'route' => 'users.show',
                    'extractor' => ArraySerializableHydrator::class,
                ],
                [
                    '__class__'           => RouteBasedCollectionMetadata::class,
                    'collection_class'    => ParticipantPaginator::class,
                    'collection_relation' => 'participants',
                    'route'               => 'participants.list',
                ],
                [
                    '__class__'           => RouteBasedCollectionMetadata::class,
                    'collection_class'    => FollowerPaginator::class,
                    'collection_relation' => 'followers',
                    'route'               => 'users.followers',
                ],
                [
                    '__class__'           => RouteBasedCollectionMetadata::class,
                    'collection_class'    => FolloweePaginator::class,
                    'collection_relation' => 'followees',
                    'route'               => 'users.followees',
                ],
            ],
            'rest_api_max_page_size' => 100,
            'rest_api_default_page_size' => 20,
        ];
    }
}
