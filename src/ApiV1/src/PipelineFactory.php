<?php
namespace ApiV1;

use Interop\Container\ContainerInterface;
use Tuupola\Middleware\CorsMiddleware;
use Zend\Expressive\Authentication\AuthenticationMiddleware;
use Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware;
use Zend\Expressive\MiddlewareFactory;
use Zend\Expressive\Router\Middleware as RouterMiddleware;
use Zend\Expressive\Router\RouteCollector;
use Zend\ProblemDetails\ProblemDetailsMiddleware;
use Zend\ProblemDetails\ProblemDetailsNotFoundHandler;
use Zend\Stratigility\MiddlewarePipe;

class PipelineFactory
{
    public function __invoke(ContainerInterface $container) : MiddlewarePipe
    {
        /** @var MiddlewareFactory $factory */
        $factory = $container->get(MiddlewareFactory::class);

        $pipeline = new MiddlewarePipe();

        $pipeline->pipe($factory->lazy(CorsMiddleware::class));
        $pipeline->pipe($factory->lazy(ProblemDetailsMiddleware::class));
        $pipeline->pipe($factory->lazy(RouteMiddleware::class));
        $pipeline->pipe($factory->lazy(RouterMiddleware\ImplicitHeadMiddleware::class));
//        $pipeline->pipe($factory->lazy(RouterMiddleware\ImplicitOptionsMiddleware::class));
        $pipeline->pipe($factory->lazy(RouterMiddleware\MethodNotAllowedMiddleware::class));
        $pipeline->pipe($factory->lazy(UrlHelperMiddleware::class));
        $pipeline->pipe($factory->lazy(RouterMiddleware\DispatchMiddleware::class));
        $pipeline->pipe($factory->lazy(ProblemDetailsNotFoundHandler::class));

        $router = $container->get(Router::class);
        $routes = new RouteCollector($router);

        $routes->get('/categories', $factory->lazy(Handler\CategoryCollectionShowHandler::class), 'categories.list');
        $routes->get('/categories/{id:[a-z0-9_-]+}', $factory->lazy(Handler\CategoryShowHandler::class), 'categories.show');

        $routes->get('/proposals', $factory->lazy(Handler\ProposalCollectionShowHandler::class), 'proposals.list');
        $routes->get('/proposals/{id:\d+}', $factory->lazy(Handler\ProposalShowHandler::class), 'proposals.show');
        $routes->get('/proposals/{proposalId:\d+}/comments', $factory->lazy(Handler\CommentCollectionShowHandler::class), 'proposals.comments.list');
        $routes->get('/proposals/{proposalId:\d+}/timetable', $factory->lazy(Handler\TimeCollectionShowHandler::class), 'proposals.timetable.list');
        $routes->get('/proposals/{proposalId:\d+}/participants', $factory->lazy(Handler\ParticipantCollectionShowHandler::class), 'proposals.participants.list');
        $routes->get('/proposals/{id:\d+}/like', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\ProposalLikeHandler::class)
        ), 'proposals.like');
        $routes->get('/proposals/{id:\d+}/unlike', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\ProposalUnlikeHandler::class)
        ), 'proposals.unlike');

        $routes->get('/users', $factory->lazy(Handler\UserCollectionShowHandler::class), 'users.list');
        $routes->get('/users/{id:\d+}', $factory->lazy(Handler\UserShowHandler::class), 'users.show');
        $routes->get('/users/{userId:\d+}/comments', $factory->lazy(Handler\CommentCollectionShowHandler::class), 'users.comments');
        $routes->get('/users/{userId:\d+}/proposals', $factory->lazy(Handler\ProposalCollectionShowHandler::class), 'users.proposals');
        $routes->get('/users/{userId:\d+}/followers', $factory->lazy(Handler\FollowerCollectionShowHandler::class), 'users.followers');
        $routes->get('/users/{userId:\d+}/followees', $factory->lazy(Handler\FolloweeCollectionShowHandler::class), 'users.followees');
        $routes->get('/users/{id:\d+}/follow', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\UserFollowHandler::class)
        ),'users.follow');
        $routes->get('/users/{id:\d+}/unfollow', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\UserUnfollowHandler::class)
        ),'users.unfollow');
        $routes->get('/users/{id:\d+}/block', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\UserBlockHandler::class)
        ),'users.block');
        $routes->get('/users/{id:\d+}/unblock', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\UserUnblockHandler::class)
        ),'users.unblock');

        $routes->get('/comments', $factory->lazy(Handler\CommentCollectionShowHandler::class), 'comments.list');
        $routes->get('/comments/{id:\d+}', $factory->lazy(Handler\CommentShowHandler::class), 'comments.show');
        $routes->get('/comments/{parentId:\d+}/replies', $factory->lazy(Handler\CommentCollectionShowHandler::class), 'comments.replies');
        $routes->get('/comments/{id:\d+}/parent-thread', $factory->lazy(Handler\CommentCollectionShowHandler::class), 'comments.parent_thread');

        $routes->post('/comments', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(BodyParamsMiddleware::class),
            $factory->lazy(Handler\CommentCreateHandler::class)
        ), 'comments.create');

        $routes->put('/comments/{id:\d+}', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(BodyParamsMiddleware::class),
            $factory->lazy(Handler\CommentUpdateHandler::class)
        ), 'comments.update');

        $routes->delete('/comments/{id:\d+}', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\CommentDeleteHandler::class)
        ), 'comments.delete');

        $routes->get('/comments/{id:\d+}/like', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\CommentLikeHandler::class)
        ), 'comments.like');

        $routes->get('/comments/{id:\d+}/unlike', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\CommentUnlikeHandler::class)
        ), 'comments.unlike');

        $routes->get('/timetable', $factory->lazy(Handler\TimeCollectionShowHandler::class), 'timetable.list');
        $routes->get('/timetable/{id:\d+}', $factory->lazy(Handler\TimeShowHandler::class), 'timetable.show');
        $routes->get('/timetable/{timeId:\d+}/participants', $factory->lazy(Handler\ParticipantCollectionShowHandler::class), 'timetable.participants.list');
        $routes->get('/timetable/{id:\d+}/participate', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\ParticipateHandler::class)
        ),'timetable.participate');
        $routes->get('/timetable/{id:\d+}/withdraw', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\WithdrawHandler::class)
        ),'timetable.withdraw');

        $routes->get('/participants', $factory->lazy(Handler\ParticipantCollectionShowHandler::class), 'participants.list');

        $routes->get('/assistants', $factory->callable(function () {}), 'assistants.list');

        $routes->post('/login', $factory->pipeline(
            $factory->lazy(BodyParamsMiddleware::class),
            $factory->lazy(Handler\LoginHandler::class)
        ),'login');

        $routes->post('/users', $factory->pipeline(
            $factory->lazy(BodyParamsMiddleware::class),
            $factory->lazy(Handler\RegisterHandler::class)
        ),'users.create');

        $routes->put('/users/{id:\d+}', $factory->pipeline(
            $factory->lazy(BodyParamsMiddleware::class),
            $factory->lazy(Handler\UserUpdateHandler::class)
        ), 'users.update');

        $routes->get('/users/self', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\UserShowHandler::class)
        ),'users.self');

        $routes->put('/users/self', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(BodyParamsMiddleware::class),
            $factory->lazy(Handler\UserSelfUpdateHandler::class)
        ),'users.self.update');

        $routes->put('/users/self/change-password', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(BodyParamsMiddleware::class),
            $factory->lazy(Handler\PasswordChangeHandler::class)
        ),'users.self.change-password');

        $routes->get('/users/self/proposals', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\ProposalCollectionShowHandler::class)
        ),'users.self.proposals');

        $routes->get('/users/self/liked-proposals', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\LikedProposalCollectionShowHandler::class)
        ),'users.self.liked_proposals');

        $routes->get('/users/self/followers', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\FollowerCollectionShowHandler::class)
        ),'users.self.followers');

        $routes->get('/users/self/followees', $factory->pipeline(
            $factory->lazy(AuthenticationMiddleware::class),
            $factory->lazy(Handler\FolloweeCollectionShowHandler::class)
        ),'users.self.followees');

        return $pipeline;
    }
}