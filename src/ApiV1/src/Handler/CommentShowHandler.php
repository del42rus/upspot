<?php
declare(strict_types=1);

namespace ApiV1\Handler;

use App\Domain\Entity\Comment;
use App\Domain\Repository\CommentRepositoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;

use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class CommentShowHandler implements RequestHandlerInterface
{
    private $commentRepository;

    private $resourceGenerator;

    private $responseFactory;

    public function __construct(
        CommentRepositoryInterface $commentRepository,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory
    ){
        $this->commentRepository = $commentRepository;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $id = $request->getAttribute('id', false);
        if (! $id) {
            throw new RuntimeException('No comment identifier provided', 400);
        }

        /** @var Comment $comment */
        $comment = $this->commentRepository->getById($id);

        $resource = $this->resourceGenerator->fromObject($comment, $request);

        return $this->responseFactory->createResponse($request, $resource);
    }
}