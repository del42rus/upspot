<?php
declare(strict_types=1);

namespace ApiV1\Handler;

use App\Domain\Entity\Time;
use App\Domain\Repository\TimeRepositoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;

use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class TimeShowHandler implements RequestHandlerInterface
{
    private $timeRepository;

    private $resourceGenerator;

    private $responseFactory;

    public function __construct(
        TimeRepositoryInterface $timeRepository,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory
    ){
        $this->timeRepository = $timeRepository;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $id = $request->getAttribute('id', false);
        if (! $id) {
            throw new RuntimeException('No time identifier provided', 400);
        }

        /** @var Time $time */
        $time = $this->timeRepository->getById($id);

        $resource = $this->resourceGenerator->fromObject($time, $request);

        return $this->responseFactory->createResponse($request, $resource);
    }
}