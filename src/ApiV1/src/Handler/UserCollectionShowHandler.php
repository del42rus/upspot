<?php

namespace ApiV1\Handler;

use App\Domain\Collection\UserPaginator;
use App\Domain\Repository\UserRepositoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class UserCollectionShowHandler implements RequestHandlerInterface
{
    private $userRepository;

    private $resourceGenerator;

    private $responseFactory;

    private $config;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory,
        $config
    ){
        $this->userRepository = $userRepository;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
        $this->config = $config;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $metadataMap = $this->resourceGenerator->getMetadataMap();

        $metadata    = $metadataMap->get(UserPaginator::class);
        $metadataQuery = $metadata->getQueryStringArguments();

        $queryParams = $request->getQueryParams();
        $page = $queryParams['page'] ?? 1;

        $itemCountPerPage = isset($queryParams['per_page']) && $queryParams['per_page'] <= $this->config['rest_api_max_page_size']
            ? $queryParams['per_page']
            : $this->config['rest_api_default_page_size'];

        $collection = $this->userRepository->getUsers($page, $itemCountPerPage);

        if (isset($queryParams['per_page'])) {
            $metadataQuery = array_merge($metadataQuery, ['per_page' => $queryParams['per_page']]);
        }

        $metadata->setQueryStringArguments($metadataQuery);

        $resource  = $this->resourceGenerator->fromObject($collection, $request);

        return $this->responseFactory->createResponse($request, $resource);
    }
}