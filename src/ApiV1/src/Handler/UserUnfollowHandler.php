<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\EntityNotFoundException;
use App\Domain\Entity\User;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Authentication\UserInterface;

class UserUnfollowHandler implements RequestHandlerInterface
{
    private $userService;

    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var User $follower */
        $follower = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute('id'));

        if (!$user) {
            throw new EntityNotFoundException();
        }

        $user->removeFollower($follower);

        $this->userService->persist($user);

        return (new Response())
            ->withHeader('Content-Type','application/json')
            ->withStatus(204);
    }
}