<?php
declare(strict_types=1);

namespace ApiV1\Handler;

use App\Domain\Entity\Category;
use App\Domain\Repository\CategoryRepositoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;

use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class CategoryShowHandler implements RequestHandlerInterface
{
    private $categoryRepository;

    private $resourceGenerator;

    private $responseFactory;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory
    ){
        $this->categoryRepository = $categoryRepository;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $slugOrId = $request->getAttribute('id', false);

        if (!$slugOrId) {
            throw new RuntimeException('No category identifier or slug provided', 400);
        }

        if (preg_match('/\d+/', $slugOrId)) {
            /** @var Category $category */
            $category = $this->categoryRepository->getById($slugOrId);
        } else {
            /** @var Category $category */
            $category = $this->categoryRepository->getOneBy(['slug' => $slugOrId]);
        }

        $resource = $this->resourceGenerator->fromObject($category, $request);

        return $this->responseFactory->createResponse($request, $resource);
    }
}