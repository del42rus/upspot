<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use App\Domain\Entity\Category;
use App\Domain\Collection\CategoryCollection;
use App\Domain\Repository\CategoryRepositoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class CategoryCollectionShowHandler implements RequestHandlerInterface
{
    private $categoryRepository;

    private $resourceGenerator;

    private $responseFactory;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory
    ){
        $this->categoryRepository = $categoryRepository;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
    }
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $result = $this->categoryRepository->getAll();

        $collection = new CategoryCollection($result);

        $resource  = $this->resourceGenerator->fromObject($collection, $request);

        return $this->responseFactory->createResponse($request, $resource);
    }
}