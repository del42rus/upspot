<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use App\Domain\Collection\FolloweePaginator;
use App\Domain\Collection\FollowerPaginator;
use App\Domain\Collection\UserCollection;
use App\Domain\Collection\UserPaginator;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Authentication\UserInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class FolloweeCollectionShowHandler implements RequestHandlerInterface
{
    private $userService;

    private $resourceGenerator;

    private $responseFactory;

    private $config;

    public function __construct(
        UserServiceInterface $userService,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory,
        $config
    ){
        $this->userService = $userService;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
        $this->config = $config;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $metadataMap = $this->resourceGenerator->getMetadataMap();

        $metadata    = $metadataMap->get(FolloweePaginator::class);
        $metadataQuery = $metadata->getQueryStringArguments();

        $queryParams = $request->getQueryParams();

        $page = $queryParams['page'] ?? 1;
        $itemCountPerPage = isset($queryParams['per_page']) && $queryParams['per_page'] <= $this->config['rest_api_max_page_size']
            ? $queryParams['per_page']
            : $this->config['rest_api_default_page_size'];

        $userId = $request->getAttribute(UserInterface::class)
                ? $request->getAttribute(UserInterface::class)->getIdentity()
                : $request->getAttribute('userId');

        $collection = $this->userService->getFolloweesByUserId($userId, $page, $itemCountPerPage);

        if (isset($queryParams['per_page'])) {
            $metadataQuery = array_merge($metadataQuery, ['per_page' => $queryParams['per_page']]);
        }

        $metadata->setQueryStringArguments($metadataQuery);
        $metadata->setRouteParams([
            'userId' => $userId
        ]);

        $resource = $this->resourceGenerator->fromObject($collection, $request);

        return $this->responseFactory->createResponse($request, $resource);
    }
}