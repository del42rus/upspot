<?php
declare(strict_types=1);

namespace ApiV1\Handler;

use App\Domain\Entity\Proposal;
use App\Domain\Entity\User;
use App\Service\ProposalServiceInterface;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;

use Zend\Expressive\Authentication\AuthenticationInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class ProposalShowHandler implements RequestHandlerInterface
{
    private $auth;
    
    private $userService;
    
    private $proposalService;

    private $resourceGenerator;

    private $responseFactory;

    public function __construct(
        AuthenticationInterface $auth,
        UserServiceInterface $userService,
        ProposalServiceInterface $proposalService,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory
    ){
        $this->auth = $auth;
        $this->userService = $userService;
        $this->proposalService = $proposalService;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $id = $request->getAttribute('id', false);
        if (! $id) {
            throw new RuntimeException('No proposal identifier provided', 400);
        }

        /** @var Proposal $proposal */
        $proposal = $this->proposalService->getById($id);

        $resource = $this->resourceGenerator->fromObject($proposal, $request);

        if ($user = $this->auth->authenticate($request)) {
            /** @var User $user */
            $user = $this->userService->getById($user->getIdentity());

            $resource = $resource->withElement('is_liked', $user->getLikedProposals()->contains($proposal));
        };

        return $this->responseFactory->createResponse($request, $resource);
    }
}