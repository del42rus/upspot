<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\EntityNotFoundException;
use App\Domain\Entity\User;
use App\Service\UserServiceInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Authentication\UserInterface;

class UserUnblockHandler implements RequestHandlerInterface
{
    private $userService;

    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var User $authenticatedUser */
        $authenticatedUser = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute('id'));

        if (!$user) {
            throw new EntityNotFoundException();
        }

        $authenticatedUser->unblockUser($user);

        try {
            $this->userService->persist($authenticatedUser);
        } catch (UniqueConstraintViolationException $exception) {

        }

        return (new Response())
            ->withHeader('Content-Type','application/json')
            ->withStatus(204);
    }
}