<?php

declare(strict_types=1);

namespace ApiV1\Handler;


use ApiV1\Handler\Exception\ForbiddenException;
use ApiV1\Handler\Exception\EntityNotFoundException;
use App\Domain\Entity\Comment;
use App\Domain\Entity\Proposal;
use App\Domain\Entity\User;
use App\Service\CommentServiceInterface;
use App\Service\ProposalServiceInterface;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Authentication\UserInterface;

class CommentDeleteHandler implements RequestHandlerInterface
{
    private $userService;

    private $commentService;

    private $proposalService;

    public function __construct(
        UserServiceInterface $userService,
        CommentServiceInterface $commentService,
        ProposalServiceInterface $proposalService
    ){
        $this->userService = $userService;
        $this->commentService = $commentService;
        $this->proposalService = $proposalService;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var Comment $comment */
        $comment = $this->commentService->getById($request->getAttribute('id'));

        if (!$comment) {
            throw new EntityNotFoundException();
        }

        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        if ($user->getId() != $comment->getAuthor()->getId()) {
            throw new ForbiddenException('The comment does not belong to you');
        }

        if ($comment->hasReplies()) {
            throw new ForbiddenException('The comment has replies');
        }

        /** @var Comment $parentComment */
        $parentComment = $comment->getParent();

        if ($parentComment) {
            $parentComment->setRepliesCount($parentComment->getRepliesCount() - 1);

            $this->commentService->persist($parentComment);
        } else {
            /** @var Proposal $proposal */
            $proposal = $comment->getProposal();

            $proposal->setCommentsCount($proposal->getCommentsCount() - 1);

            $this->proposalService->persist($proposal);
        }

        $this->commentService->remove($comment);

        return (new Response())
            ->withHeader('Content-Type','application/json')
            ->withStatus(204);
    }
}