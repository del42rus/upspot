<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\ForbiddenException;
use ApiV1\Handler\Exception\EntityNotFoundException;
use ApiV1\Handler\Exception\ValidationFailedException;
use App\Domain\Entity\Comment;
use App\Domain\Entity\User;
use App\InputFilter\CommentInputFilter;
use App\Service\CommentServiceInterface;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Authentication\UserInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class CommentUpdateHandler implements RequestHandlerInterface
{
    private $userService;

    private $commentService;

    private $inputFilter;

    private $resourceGenerator;

    private $responseFactory;

    public function __construct(
        UserServiceInterface $userService,
        CommentServiceInterface $commentService,
        CommentInputFilter $inputFilter,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory
    ){
        $this->userService = $userService;
        $this->commentService = $commentService;
        $this->inputFilter = $inputFilter;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var Comment $comment */
        $comment = $this->commentService->getById($request->getAttribute('id'));

        if (!$comment) {
            throw new EntityNotFoundException();
        }

        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        if ($user->getId() != $comment->getAuthor()->getId()) {
            throw new ForbiddenException('The comment does not belong to you');
        }

        if ($comment->hasReplies()) {
            throw new ForbiddenException('The comment has replies');
        }

        $post = $request->getParsedBody();

        $this->inputFilter->setData($post);

        if (!$this->inputFilter->isValid()) {
            throw new ValidationFailedException($this->inputFilter->getMessages());
        }

        $comment->setText($this->inputFilter->getValue('text'));

        $this->commentService->persist($comment);

        $resource = $this->resourceGenerator->fromObject($comment, $request);

        return $this->responseFactory->createResponse($request, $resource);
    }
}