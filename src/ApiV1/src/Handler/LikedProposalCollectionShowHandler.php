<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use App\Domain\Collection\ProposalPaginator;
use App\Service\ProposalServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Authentication\UserInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class LikedProposalCollectionShowHandler implements RequestHandlerInterface
{
    private $proposalService;

    private $resourceGenerator;

    private $responseFactory;

    private $config;

    public function __construct(
        ProposalServiceInterface $proposalService,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory,
        $config
    ){
        $this->proposalService = $proposalService;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
        $this->config = $config;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $metadataMap = $this->resourceGenerator->getMetadataMap();

        $metadata    = $metadataMap->get(ProposalPaginator::class);
        $metadataQuery = $metadata->getQueryStringArguments();

        $queryParams = $request->getQueryParams();

        $page = $queryParams['page'] ?? 1;
        $itemCountPerPage = isset($queryParams['per_page']) && $queryParams['per_page'] <= $this->config['rest_api_max_page_size']
            ? $queryParams['per_page']
            : $this->config['rest_api_default_page_size'];

        $orderBy = [];

        $userId = $request->getAttribute(UserInterface::class)->getIdentity();

        $collection = $this->proposalService->getLikedProposalsByUserId($userId, $orderBy, $page, $itemCountPerPage);

        if (isset($queryParams['per_page'])) {
            $metadataQuery = array_merge($metadataQuery, ['per_page' => $queryParams['per_page']]);
        }

        $metadata->setQueryStringArguments($metadataQuery);

        $resource = $this->resourceGenerator->fromObject($collection, $request);

        return $this->responseFactory->createResponse($request, $resource);
    }
}