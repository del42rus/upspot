<?php

declare(strict_types=1);

namespace ApiV1\Handler\Exception;

use Zend\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;
use Zend\ProblemDetails\Exception\ProblemDetailsExceptionInterface;

class EntityNotFoundException extends \Exception implements ProblemDetailsExceptionInterface
{
    use CommonProblemDetailsExceptionTrait;

    public function __construct()
    {
        $this->status = 404;
        $this->title = '';
        $this->type = '';
        $this->detail = 'Entity not found.';
    }

}