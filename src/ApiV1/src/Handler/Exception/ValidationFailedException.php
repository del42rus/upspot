<?php

namespace ApiV1\Handler\Exception;

use Zend\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;
use Zend\ProblemDetails\Exception\ProblemDetailsExceptionInterface;

class ValidationFailedException extends \Exception implements ProblemDetailsExceptionInterface
{
    use CommonProblemDetailsExceptionTrait;

    public function __construct($errors)
    {
        $this->status = 422;
        $this->title = '';
        $this->type = '';
        $this->detail = 'Validation failed.';

        if (count($errors)) {
            $this->additional['errors'] = $errors;
        }
    }
}