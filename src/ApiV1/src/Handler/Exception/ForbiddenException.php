<?php

declare(strict_types=1);

namespace ApiV1\Handler\Exception;

use Zend\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;
use Zend\ProblemDetails\Exception\ProblemDetailsExceptionInterface;

class ForbiddenException extends \Exception implements ProblemDetailsExceptionInterface
{
    use CommonProblemDetailsExceptionTrait;

    public function __construct($message)
    {
        $this->status = 403;
        $this->title = 'Forbidden.';
        $this->type = '';
        $this->detail = $message;
    }

}