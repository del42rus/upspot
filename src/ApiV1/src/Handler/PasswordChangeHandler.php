<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\EntityNotFoundException;
use ApiV1\Handler\Exception\ValidationFailedException;
use App\Domain\Entity\User;
use App\InputFilter\PasswordChangeInputFilter;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Authentication\UserInterface;
use Zend\Validator\Callback;

class PasswordChangeHandler implements RequestHandlerInterface
{
    private $userService;

    private $inputFilter;

    public function __construct(
        UserServiceInterface $userService,
        PasswordChangeInputFilter $inputFilter
    ){
        $this->userService = $userService;
        $this->inputFilter = $inputFilter;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        if (!$user) {
            throw new EntityNotFoundException();
        }

        $validator = new Callback(function ($value) use ($user) {
            return password_verify($value, $user->getPassword());
        });

        $this->inputFilter->get('old_password')
            ->getValidatorChain()
            ->addValidator($validator);

        $post = $request->getParsedBody();

        $this->inputFilter->setData($post);

        if (!$this->inputFilter->isValid()) {
            throw new ValidationFailedException($this->inputFilter->getMessages());
        }

        $user->setPassword($this->inputFilter->getValue('new_password'));

        $this->userService->persist($user);

        return (new Response())
            ->withHeader('Content-Type','application/json')
            ->withStatus(204);
    }
}