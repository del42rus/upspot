<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\EntityNotFoundException;
use App\Domain\Entity\Time;
use App\Domain\Entity\User;
use App\Service\TimeServiceInterface;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Authentication\UserInterface;

class WithdrawHandler implements RequestHandlerInterface
{
    private $userService;

    private $timeService;

    public function __construct(
        UserServiceInterface $userService,
        TimeServiceInterface $timeService
    ){
        $this->userService = $userService;
        $this->timeService = $timeService;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        /** @var Time $time */
        $time = $this->timeService->getById($request->getAttribute('id'));

        if (!$time) {
            throw new EntityNotFoundException();
        }

        $time->removeParticipant($user);

        if ($user->isMale()) {
            $time->setBoysCount($time->getBoysCount() - 1);
        } else {
            $time->setGirlsCount($time->getGirlsCount() - 1);
        }

        $this->timeService->persist($time);

        return (new Response())
            ->withHeader('Content-Type','application/json')
            ->withStatus(204);
    }
}