<?php
declare(strict_types=1);

namespace ApiV1\Handler;

use App\Domain\Entity\User;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;
use Zend\Expressive\Authentication\AuthenticationInterface;
use Zend\Expressive\Authentication\UserInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class UserShowHandler implements RequestHandlerInterface
{
    private $auth;
    
    private $userService;

    private $resourceGenerator;

    private $responseFactory;

    public function __construct(
        AuthenticationInterface $auth,
        UserServiceInterface $userService,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory
    ){
        $this->auth = $auth;
        $this->userService = $userService;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $id = $request->getAttribute('id', false) ?: $request->getAttribute(UserInterface::class)->getIdentity();

        if (! $id) {
            throw new RuntimeException('No user identifier provided', 400);
        }

        /** @var User $user */
        $user = $this->userService->getById($id);

        $resource = $this->resourceGenerator->fromObject($user, $request);
        $authenticatedUser = $this->auth->authenticate($request);

        if ($authenticatedUser && $authenticatedUser->getIdentity() != $user->getId()) {
            /** @var User $authenticatedUser */
            $authenticatedUser = $this->userService->getById($authenticatedUser->getIdentity());

            $resource = $resource->withElements([
                'is_followed' => $user->getFollowers()->contains($authenticatedUser),
                'is_blocked' => $authenticatedUser->getBlockedUsers()->contains($user)
            ]);
        }
        
        return $this->responseFactory->createResponse($request, $resource);
    }
}