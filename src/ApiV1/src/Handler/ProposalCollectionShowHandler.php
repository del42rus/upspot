<?php

namespace ApiV1\Handler;

use App\Domain\Entity\Category;
use App\Domain\Collection\ProposalPaginator;
use App\Domain\Entity\Proposal;
use App\Domain\Entity\User;
use App\Service\ProposalServiceInterface;
use App\Persistence\Doctrine\Repository\CategoryRepository;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Authentication\AuthenticationInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class ProposalCollectionShowHandler implements RequestHandlerInterface
{
    private $userService;

    private $proposalService;

    private $categoryRepository;

    private $resourceGenerator;

    private $responseFactory;

    private $auth;

    private $config;

    public function __construct(
        UserServiceInterface $userService,
        ProposalServiceInterface $proposalService,
        CategoryRepository $categoryRepository,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory,
        AuthenticationInterface $auth,
        $config
    ){
        $this->userService = $userService;
        $this->proposalService = $proposalService;
        $this->categoryRepository = $categoryRepository;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
        $this->auth = $auth;
        $this->config = $config;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $metadataMap = $this->resourceGenerator->getMetadataMap();

        $metadata    = $metadataMap->get(ProposalPaginator::class);
        $metadataQuery = $metadata->getQueryStringArguments();

        $queryParams = $request->getQueryParams();
        $queryParams['include_child_categories'] = $queryParams['include_child_categories'] ?? 0;

        $page = $queryParams['page'] ?? 1;
        $itemCountPerPage = isset($queryParams['per_page']) && $queryParams['per_page'] <= $this->config['rest_api_max_page_size']
            ? $queryParams['per_page']
            : $this->config['rest_api_default_page_size'];

        $params = $orderBy = [];

        if (isset($queryParams['sort_by']) &&
            preg_match('/(?P<sortField>\w+)\.(?P<sortOrder>\w+)/', $queryParams['sort_by'], $match)
        ) {
            $orderBy = [$match['sortField'] => $match['sortOrder']];
        }

        if ($user = $this->auth->authenticate($request)) {
            /** @var User $user */
            $user = $this->userService->getById($user->getIdentity());
        };

        $params['userId'] = $request->getAttribute('userId') ?: ($queryParams['user_id'] ?? null);

        if (isset($queryParams['category_id'])) {
            $metadataQuery = array_merge($metadataQuery, ['category_id' => $queryParams['category_id']]);

            /** @var Category $category */
            $category = $this->categoryRepository->getById($queryParams['category_id']);

            $categoryIds = [$category->getId()];

            if ($queryParams['include_child_categories']) {
                $metadataQuery = array_merge($metadataQuery, ['include_child_categories' => $queryParams['include_child_categories']]);

                $categoryIds = array_merge($categoryIds, $category->getChildrenIds());
            }

            $params['categoryIds'] = $categoryIds;

            $collection = $this->proposalService->getProposals($params, $orderBy, $page, $itemCountPerPage);
        } elseif (isset($queryParams['category_slug'])) {
            $metadataQuery = array_merge($metadataQuery, ['category_slug' => $queryParams['category_slug']]);

            /** @var Category $category */
            $category = $this->categoryRepository->getOneBy(['slug' => $queryParams['category_slug']]);

            $categoryIds = [$category->getId()];

            if ($queryParams['include_child_categories']) {
                $metadataQuery = array_merge($metadataQuery, ['include_child_categories' => $queryParams['include_child_categories']]);

                $categoryIds = array_merge($categoryIds, $category->getChildrenIds());
            }

            $params['categoryIds'] = $categoryIds;

            $collection = $this->proposalService->getProposals($params, $orderBy, $page, $itemCountPerPage);
        } else {
            $collection = $this->proposalService->getProposals($params, $orderBy, $page, $itemCountPerPage);
        }

        if (isset($queryParams['per_page'])) {
            $metadataQuery = array_merge($metadataQuery, ['per_page' => $queryParams['per_page']]);
        }

        $metadata->setQueryStringArguments($metadataQuery);

        $resource = $this->resourceGenerator->fromObject($collection, $request);

        if (isset($user) && count($collection)) {
            $data = $resource->toArray();

            foreach ($data['_embedded'][$metadata->getCollectionRelation()] as &$item) {
                $item['is_liked'] = $user->getLikedProposals()->exists(function ($key, Proposal $element) use ($item) {
                    return $element->getId() == $item['id'];
                });
            }

            return (new JsonResponse($data))->withHeader('Content-Type', 'application/hal+json');
        }

        return $this->responseFactory->createResponse($request, $resource);
    }
}