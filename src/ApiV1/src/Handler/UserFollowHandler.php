<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\EntityNotFoundException;
use App\Domain\Entity\User;
use App\Service\UserServiceInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Authentication\UserInterface;
use Zend\ProblemDetails\ProblemDetailsResponseFactory;

class UserFollowHandler implements RequestHandlerInterface
{
    private $userService;

    private $problemDetailsFactory;

    public function __construct(
        UserServiceInterface $userService,
        ProblemDetailsResponseFactory $problemDetailsFactory
    ){
        $this->userService = $userService;
        $this->problemDetailsFactory = $problemDetailsFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var User $follower */
        $follower = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute('id'));

        if (!$user) {
            throw new EntityNotFoundException();
        }

        if ($user->getId() == $follower->getId()) {
            return $this->problemDetailsFactory->createResponse(
                $request,
                400,
                'You cannot follow yourself'
            );
        }

        if ($user->getBlockedUsers()->contains($follower)) {
            return $this->problemDetailsFactory->createResponse(
                $request,
                422,
                'You cannot follow the user'
            );
        }

        $user->addFollower($follower);

        try {
            $this->userService->persist($user);
        } catch (UniqueConstraintViolationException $exception) {

        }

        return (new Response())
            ->withHeader('Content-Type','application/json')
            ->withStatus(204);
    }
}