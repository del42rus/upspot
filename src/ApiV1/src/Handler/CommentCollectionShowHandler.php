<?php

namespace ApiV1\Handler;

use App\Domain\Collection\CommentCollection;
use App\Domain\Collection\CommentPaginator;
use App\Domain\Collection\CommentParentThreadCollection;
use App\Domain\Entity\Comment;
use App\Domain\Entity\User;
use App\Service\CommentServiceInterface;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Authentication\AuthenticationInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\Metadata\RouteBasedCollectionMetadata;
use Zend\Expressive\Hal\ResourceGenerator;

class CommentCollectionShowHandler implements RequestHandlerInterface
{
    private $userService;

    private $commentService;

    private $resourceGenerator;

    private $responseFactory;

    private $auth;

    private $config;

    public function __construct(
        UserServiceInterface $userService,
        CommentServiceInterface $commentService,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory,
        AuthenticationInterface $auth,
        $config
    ){
        $this->userService = $userService;
        $this->commentService = $commentService;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
        $this->auth = $auth;
        $this->config = $config;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $metadataMap = $this->resourceGenerator->getMetadataMap();

        /** @var RouteBasedCollectionMetadata $metadata */
        $metadata    = $metadataMap->get(CommentPaginator::class);
        $metadataQuery = $metadata->getQueryStringArguments();
        $queryParams = $request->getQueryParams();

        $page = $queryParams['page'] ?? 1;

        $itemCountPerPage = isset($queryParams['per_page']) && $queryParams['per_page'] <= $this->config['rest_api_max_page_size']
            ? $queryParams['per_page']
            : $this->config['rest_api_default_page_size'];

        $proposalId = $request->getAttribute('proposalId') ?: ($queryParams['proposal_id'] ?? null);
        $userId = $request->getAttribute('userId') ?: ($queryParams['user_id'] ?? null);
        $parentId = $request->getAttribute('parentId') ?: ($queryParams['parent_id'] ?? null);
        $commentId = $request->getAttribute('id') ?: ($queryParams['comment_id'] ?? null);

        if ($proposalId) {
            $collection = $this->commentService->getCommentsByProposalId($proposalId, $page, $itemCountPerPage);
            $metadataQuery = array_merge($metadataQuery, ['proposal_id' => $proposalId]);
        } elseif (isset($userId)) {
            $collection = $this->commentService->getCommentsByUserId($userId, $page, $itemCountPerPage);
            $metadataQuery = array_merge($metadataQuery, ['user_id' => $userId]);
        } elseif ($parentId) {
            $collection = $this->commentService->getRepliesByCommentId($parentId, $page, $itemCountPerPage);
            $metadataQuery = array_merge($metadataQuery, ['parent_id' => $parentId]);
        } elseif ($commentId) {
            $metadata    = $metadataMap->get(CommentParentThreadCollection::class);
            $metadataQuery = $metadata->getQueryStringArguments();

            /** @var Comment $comment */
            $comment = $this->commentService->getById($commentId);

            $collection = $comment->getParentThread();
        } else {
            $collection = $this->commentService->getComments($page, $itemCountPerPage);
        }

        if (isset($queryParams['per_page'])) {
            $metadataQuery = array_merge($metadataQuery, ['per_page' => $queryParams['per_page']]);
        }

        $metadata->setQueryStringArguments($metadataQuery);

        $resource = $this->resourceGenerator->fromObject($collection, $request);

        if (($user = $this->auth->authenticate($request)) && count($collection)) {
            $data = $resource->toArray();

            /** @var User $user */
            $user = $this->userService->getById($user->getIdentity());

            foreach ($data['_embedded'][$metadata->getCollectionRelation()] as &$item) {
                $item['is_liked'] = $user->getLikedComments()->exists(function ($key, Comment $element) use ($item) {
                    return $element->getId() == $item['id'];
                });
            }

            return (new JsonResponse($data))->withHeader('Content-Type', 'application/hal+json');
        }

        return $this->responseFactory->createResponse($request, $resource);
    }
}