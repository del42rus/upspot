<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\EntityNotFoundException;
use App\Domain\Entity\Time;
use App\Domain\Entity\User;
use App\Service\TimeServiceInterface;
use App\Service\UserServiceInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Authentication\UserInterface;
use Zend\ProblemDetails\ProblemDetailsResponseFactory;

class ParticipateHandler implements RequestHandlerInterface
{
    private $userService;

    private $timeService;

    private $problemDetailsFactory;

    public function __construct(
        UserServiceInterface $userService,
        TimeServiceInterface $timeService,
        ProblemDetailsResponseFactory $problemDetailsFactory
    ){
        $this->userService = $userService;
        $this->timeService = $timeService;
        $this->problemDetailsFactory = $problemDetailsFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        if ($user->getGender() === null) {
            return $this->problemDetailsFactory->createResponse(
                $request,
                422,
                'Unknown gender of the user'
            );
        }

        /** @var Time $time */
        $time = $this->timeService->getById($request->getAttribute('id'));

        if (!$time) {
            throw new EntityNotFoundException();
        }

        $proposal = $time->getProposal();

        if ($proposal->getMaxBoysCount() &&
            $time->getBoysCount() == $proposal->getMaxBoysCount() &&
            $user->isMale()) {
            return $this->problemDetailsFactory->createResponse(
                $request,
                422,
                'No places left for boys'
            );
        }

        if ($proposal->getMaxGirlsCount() &&
            $time->getGirlsCount() == $proposal->getMaxGirlsCount() &&
            $user->isFemale()) {
            return $this->problemDetailsFactory->createResponse(
                $request,
                422,
                'No places left for girls'
            );
        }

        if ($proposal->getMaxPeopleCount() == $time->getPeopleCount()) {
            return $this->problemDetailsFactory->createResponse(
                $request,
                422,
                'No places left'
            );
        }

        $time->addParticipant($user);

        if ($user->isMale()) {
            $time->setBoysCount($time->getBoysCount() + 1);
        } else {
            $time->setGirlsCount($time->getGirlsCount() + 1);
        }

        try {
            $this->timeService->persist($time);
        } catch (UniqueConstraintViolationException $exception) {

        }

        return (new Response())
            ->withHeader('Content-Type','application/json')
            ->withStatus(204);
    }
}