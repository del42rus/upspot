<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\UserUnblockHandler;
use App\Service\UserServiceInterface;
use Psr\Container\ContainerInterface;

class UserUnblockHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        return new UserUnblockHandler($userService);
    }
}