<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\ProposalUnlikeHandler;
use App\Service\ProposalServiceInterface;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;

class ProposalUnlikeHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var ProposalServiceInterface $proposalService */
        $proposalService = $container->get(ProposalServiceInterface::class);

        return new ProposalUnlikeHandler(
            $userService,
            $proposalService
        );
    }
}