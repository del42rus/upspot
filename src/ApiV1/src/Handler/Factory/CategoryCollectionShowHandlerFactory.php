<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use App\Domain\Repository\CategoryRepositoryInterface;
use ApiV1\Handler\CategoryCollectionShowHandler;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;
use Zend\ServiceManager\Factory\FactoryInterface;

class CategoryCollectionShowHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var CategoryRepositoryInterface $categoryRepository */
        $categoryRepository = $container->get(CategoryRepositoryInterface::class);

        /** @var ResourceGenerator $resourceGenerator */
        $resourceGenerator = $container->get(\ApiV1\ResourceGenerator::class);

        /** @var HalResponseFactory $responseFactory */
        $responseFactory = $container->get(HalResponseFactory::class);

        return new CategoryCollectionShowHandler(
            $categoryRepository,
            $resourceGenerator,
            $responseFactory
        );
    }
}