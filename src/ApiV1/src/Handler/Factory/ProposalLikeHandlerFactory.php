<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;


use ApiV1\Handler\ProposalLikeHandler;
use App\Service\ProposalServiceInterface;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;
use Zend\ProblemDetails\ProblemDetailsResponseFactory;

class ProposalLikeHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var ProposalServiceInterface $proposalService */
        $proposalService = $container->get(ProposalServiceInterface::class);

        /** @var ProblemDetailsResponseFactory $problemDetailsFactory */
        $problemDetailsFactory = $container->get(ProblemDetailsResponseFactory::class);

        return new ProposalLikeHandler(
            $userService,
            $proposalService,
            $problemDetailsFactory
        );
    }
}