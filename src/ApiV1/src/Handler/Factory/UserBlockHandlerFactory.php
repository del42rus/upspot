<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\UserBlockHandler;
use App\Service\UserServiceInterface;
use Psr\Container\ContainerInterface;
use Zend\ProblemDetails\ProblemDetailsResponseFactory;

class UserBlockHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var ProblemDetailsResponseFactory $problemDetailsFactory */
        $problemDetailsFactory = $container->get(ProblemDetailsResponseFactory::class);

        return new UserBlockHandler(
            $userService,
            $problemDetailsFactory
        );
    }
}