<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use App\Domain\Repository\UserRepositoryInterface;
use ApiV1\Handler\UserCollectionShowHandler;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserCollectionShowHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var UserRepositoryInterface $userRepository */
        $userRepository = $container->get(UserRepositoryInterface::class);

        /** @var ResourceGenerator $resourceGenerator */
        $resourceGenerator = $container->get(\ApiV1\ResourceGenerator::class);

        /** @var HalResponseFactory $responseFactory */
        $responseFactory = $container->get(HalResponseFactory::class);

        return new UserCollectionShowHandler(
            $userRepository,
            $resourceGenerator,
            $responseFactory,
            $container->get('config')
        );
    }
}