<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\PasswordChangeHandler;
use App\InputFilter\PasswordChangeInputFilter;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;

class PasswordChangeHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var PasswordChangeInputFilter $inputFilter */
        $inputFilter = $container->get('InputFilterManager')->get(PasswordChangeInputFilter::class);

        return new PasswordChangeHandler(
            $userService,
            $inputFilter
        );
    }
}