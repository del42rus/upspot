<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;


use ApiV1\Handler\RegisterHandler;
use App\InputFilter\RegisterInputFilter;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class RegisterHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var RegisterInputFilter $inputFilter */
        $inputFilter = $container->get('InputFilterManager')->get(RegisterInputFilter::class);

        /** @var ResourceGenerator $resourceGenerator */
        $resourceGenerator = $container->get(\ApiV1\ResourceGenerator::class);

        /** @var HalResponseFactory $responseFactory */
        $responseFactory = $container->get(HalResponseFactory::class);

        return new RegisterHandler(
            $userService,
            $inputFilter,
            $resourceGenerator,
            $responseFactory
        );
    }
}