<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use App\Domain\Repository\CategoryRepositoryInterface;
use App\Service\ProposalServiceInterface;
use ApiV1\Handler\ProposalCollectionShowHandler;
use App\Persistence\Doctrine\Repository\CategoryRepository;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Authentication\AuthenticationInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;
use Zend\ServiceManager\Factory\FactoryInterface;

class ProposalCollectionShowHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var ProposalServiceInterface $proposalService */
        $proposalService = $container->get(ProposalServiceInterface::class);

        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $container->get(CategoryRepositoryInterface::class);

        /** @var ResourceGenerator $resourceGenerator */
        $resourceGenerator = $container->get(\ApiV1\ResourceGenerator::class);

        /** @var HalResponseFactory $responseFactory */
        $responseFactory = $container->get(HalResponseFactory::class);

        /** @var AuthenticationInterface $auth */
        $auth = $container->get(AuthenticationInterface::class);

        return new ProposalCollectionShowHandler(
            $userService,
            $proposalService,
            $categoryRepository,
            $resourceGenerator,
            $responseFactory,
            $auth,
            $container->get('config')
        );
    }
}