<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use App\Domain\Repository\TimeRepositoryInterface;
use ApiV1\Handler\TimeCollectionShowHandler;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;
use Zend\ServiceManager\Factory\FactoryInterface;

class TimeCollectionShowHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var TimeRepositoryInterface $timeRepository */
        $timeRepository = $container->get(TimeRepositoryInterface::class);

        /** @var ResourceGenerator $resourceGenerator */
        $resourceGenerator = $container->get(\ApiV1\ResourceGenerator::class);

        /** @var HalResponseFactory $responseFactory */
        $responseFactory = $container->get(HalResponseFactory::class);

        return new TimeCollectionShowHandler(
            $timeRepository,
            $resourceGenerator,
            $responseFactory,
            $container->get('config')
        );
    }
}