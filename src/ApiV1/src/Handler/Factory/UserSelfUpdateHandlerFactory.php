<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\UserSelfUpdateHandler;
use App\InputFilter\UserInputFilter;
use App\Service\UserServiceInterface;
use Psr\Container\ContainerInterface;

class UserSelfUpdateHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var UserInputFilter $inputFilter */
        $inputFilter = $container->get('InputFilterManager')->get(UserInputFilter::class);

        return new UserSelfUpdateHandler(
            $userService,
            $inputFilter
        );
    }
}