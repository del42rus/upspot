<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;


use ApiV1\Handler\UserFollowHandler;
use ApiV1\Handler\UserUnfollowHandler;
use App\Service\UserServiceInterface;
use Psr\Container\ContainerInterface;

class UserUnfollowHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        return new UserUnfollowHandler($userService);
    }
}