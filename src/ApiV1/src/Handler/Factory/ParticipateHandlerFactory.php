<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\ParticipateHandler;
use App\Service\TimeServiceInterface;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;
use Zend\ProblemDetails\ProblemDetailsResponseFactory;

class ParticipateHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var TimeServiceInterface $timeService */
        $timeService = $container->get(TimeServiceInterface::class);

        /** @var ProblemDetailsResponseFactory $problemDetailsFactory */
        $problemDetailsFactory = $container->get(ProblemDetailsResponseFactory::class);

        return new ParticipateHandler(
            $userService,
            $timeService,
            $problemDetailsFactory
        );
    }
}