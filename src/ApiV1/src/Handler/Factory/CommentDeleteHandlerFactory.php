<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;


use ApiV1\Handler\CommentDeleteHandler;
use App\Service\CommentServiceInterface;
use App\Service\ProposalServiceInterface;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;

class CommentDeleteHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var CommentServiceInterface $commentService */
        $commentService = $container->get(CommentServiceInterface::class);

        /** @var ProposalServiceInterface $proposalService */
        $proposalService = $container->get(ProposalServiceInterface::class);

        return new CommentDeleteHandler(
            $userService,
            $commentService,
            $proposalService
        );
    }
}