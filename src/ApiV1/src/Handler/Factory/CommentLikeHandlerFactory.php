<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;


use ApiV1\Handler\CommentLikeHandler;
use App\Service\CommentServiceInterface;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;
use Zend\ProblemDetails\ProblemDetailsResponseFactory;

class CommentLikeHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var CommentServiceInterface $commentService */
        $commentService = $container->get(CommentServiceInterface::class);

        /** @var ProblemDetailsResponseFactory $problemDetailsFactory */
        $problemDetailsFactory = $container->get(ProblemDetailsResponseFactory::class);

        return new CommentLikeHandler(
            $userService,
            $commentService,
            $problemDetailsFactory
        );
    }
}