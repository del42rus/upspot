<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use App\Domain\Repository\UserRepositoryInterface;
use ApiV1\Handler\UserShowHandler;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Authentication\AuthenticationInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserShowHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var AuthenticationInterface $auth */
        $auth = $container->get(AuthenticationInterface::class);

        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var ResourceGenerator $resourceGenerator */
        $resourceGenerator = $container->get(\ApiV1\ResourceGenerator::class);

        /** @var HalResponseFactory $responseFactory */
        $responseFactory = $container->get(HalResponseFactory::class);

        return new UserShowHandler(
            $auth,
            $userService,
            $resourceGenerator,
            $responseFactory
        );
    }
}