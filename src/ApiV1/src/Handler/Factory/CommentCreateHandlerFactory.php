<?php

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\CommentCreateHandler;
use App\InputFilter\CommentInputFilter;
use App\Service\CommentServiceInterface;
use App\Service\ProposalServiceInterface;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class CommentCreateHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var CommentServiceInterface $commentService */
        $commentService = $container->get(CommentServiceInterface::class);

        /** @var ProposalServiceInterface $proposalService */
        $proposalService = $container->get(ProposalServiceInterface::class);

        /** @var CommentInputFilter $commentInputFilter */
        $commentInputFilter = $container->get('InputFilterManager')->get(CommentInputFilter::class);

        /** @var ResourceGenerator $resourceGenerator */
        $resourceGenerator = $container->get(\ApiV1\ResourceGenerator::class);

        /** @var HalResponseFactory $responseFactory */
        $responseFactory = $container->get(HalResponseFactory::class);

        return new CommentCreateHandler(
            $userService,
            $commentService,
            $proposalService,
            $commentInputFilter,
            $resourceGenerator,
            $responseFactory
        );
    }
}