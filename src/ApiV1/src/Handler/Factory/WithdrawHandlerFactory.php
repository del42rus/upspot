<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\WithdrawHandler;
use App\Service\TimeServiceInterface;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;

class WithdrawHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var TimeServiceInterface $timeService */
        $timeService = $container->get(TimeServiceInterface::class);

        return new WithdrawHandler(
            $userService,
            $timeService
        );
    }
}