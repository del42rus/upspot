<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use App\Service\CommentServiceInterface;
use ApiV1\Handler\CommentCollectionShowHandler;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Authentication\AuthenticationInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;
use Zend\ServiceManager\Factory\FactoryInterface;

class CommentCollectionShowHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var CommentServiceInterface $commentService */
        $commentService = $container->get(CommentServiceInterface::class);

        /** @var ResourceGenerator $resourceGenerator */
        $resourceGenerator = $container->get(\ApiV1\ResourceGenerator::class);

        /** @var HalResponseFactory $responseFactory */
        $responseFactory = $container->get(HalResponseFactory::class);

        /** @var AuthenticationInterface $auth */
        $auth = $container->get(AuthenticationInterface::class);

        return new CommentCollectionShowHandler(
            $userService,
            $commentService,
            $resourceGenerator,
            $responseFactory,
            $auth,
            $container->get('config')
        );
    }
}