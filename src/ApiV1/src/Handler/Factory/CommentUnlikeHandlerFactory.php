<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\CommentUnlikeHandler;
use App\Service\CommentServiceInterface;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;

class CommentUnlikeHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var CommentServiceInterface $commentService */
        $commentService = $container->get(CommentServiceInterface::class);

        return new CommentUnlikeHandler(
            $userService,
            $commentService
        );
    }
}