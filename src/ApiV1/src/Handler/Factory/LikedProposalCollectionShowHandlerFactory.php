<?php

declare(strict_types=1);

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\LikedProposalCollectionShowHandler;
use App\Service\ProposalServiceInterface;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;
use Zend\ServiceManager\Factory\FactoryInterface;

class LikedProposalCollectionShowHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var ProposalServiceInterface $proposalService */
        $proposalService = $container->get(ProposalServiceInterface::class);

        /** @var ResourceGenerator $resourceGenerator */
        $resourceGenerator = $container->get(\ApiV1\ResourceGenerator::class);

        /** @var HalResponseFactory $responseFactory */
        $responseFactory = $container->get(HalResponseFactory::class);

        return new LikedProposalCollectionShowHandler(
            $proposalService,
            $resourceGenerator,
            $responseFactory,
            $container->get('config')
        );
    }
}