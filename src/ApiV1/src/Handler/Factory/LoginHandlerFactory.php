<?php

namespace ApiV1\Handler\Factory;

use ApiV1\Handler\LoginHandler;
use App\Service\UserServiceInterface;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;
use Zend\ProblemDetails\ProblemDetailsResponseFactory;

class LoginHandlerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserServiceInterface $userService */
        $userService = $container->get(UserServiceInterface::class);

        /** @var ProblemDetailsResponseFactory $problemDetailsFactory */
        $problemDetailsFactory = $container->get(ProblemDetailsResponseFactory::class);

        return new LoginHandler(
            $userService,
            $problemDetailsFactory,
            $container->get('config')['authentication']
        );
    }
}