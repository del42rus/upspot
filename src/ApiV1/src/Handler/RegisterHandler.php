<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\ValidationFailedException;
use App\Domain\Entity\User;
use App\InputFilter\RegisterInputFilter;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class RegisterHandler implements RequestHandlerInterface
{
    private $userService;

    private $inputFilter;

    private $resourceGenerator;

    private $responseFactory;

    public function __construct(
        UserServiceInterface $userService,
        RegisterInputFilter $inputFilter,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory
    ){
        $this->userService = $userService;
        $this->inputFilter = $inputFilter;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $post = $request->getParsedBody();

        $this->inputFilter->setData($post);

        if (!$this->inputFilter->isValid()) {
            throw new ValidationFailedException($this->inputFilter->getMessages());
        }

        $user = new User();

        $user->setName($this->inputFilter->getValue('name'));
        $user->setUsername($this->inputFilter->getValue('username'));
        $user->setEmail($this->inputFilter->getValue('email'));
        $user->setPassword($this->inputFilter->getValue('password'));

        $this->userService->persist($user);

        $resource = $this->resourceGenerator->fromObject($user, $request);

        return $this->responseFactory->createResponse($request, $resource);
    }
}