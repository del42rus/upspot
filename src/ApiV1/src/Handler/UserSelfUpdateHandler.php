<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\EntityNotFoundException;
use ApiV1\Handler\Exception\ValidationFailedException;
use App\Domain\Entity\User;
use App\InputFilter\UserInputFilter;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Authentication\UserInterface;
use Zend\Validator\Callback;

class UserSelfUpdateHandler implements RequestHandlerInterface
{
    private $userService;

    private $inputFilter;

    public function __construct(
        UserServiceInterface $userService,
        UserInputFilter $inputFilter
    ){
        $this->userService = $userService;
        $this->inputFilter = $inputFilter;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        if (!$user) {
            throw new EntityNotFoundException();
        }

        $post = $request->getParsedBody();

        if (isset($post['password'])) {
            $this->inputFilter->get('confirm')->setRequired(true);
        }

        $this->inputFilter->setData($post);

        if (!$this->inputFilter->isValid(['id' => $user->getId()])) {
            throw new ValidationFailedException($this->inputFilter->getMessages());
        }

        $user->setName($this->inputFilter->getValue('name'))
            ->setUsername($this->inputFilter->getValue('username'))
            ->setEmail($this->inputFilter->getValue('email'));

        if ($this->inputFilter->getValue('password')) {
            $user->setPassword($this->inputFilter->getValue('password'));
        }

        if ($this->inputFilter->getValue('gender')) {
            $user->setGender($this->inputFilter->getValue('gender'));
        }

        $this->userService->persist($user);

        return (new Response())
            ->withHeader('Content-Type','application/json')
            ->withStatus(204);
    }
}