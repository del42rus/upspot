<?php

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\InvalidLoginCredentialsException;
use App\Domain\Entity\User;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Zend\ProblemDetails\ProblemDetailsResponseFactory;

class LoginHandler implements RequestHandlerInterface
{
    private $userService;

    private $problemDetailsFactory;

    private $config;

    public function __construct(
        UserServiceInterface $userService,
        ProblemDetailsResponseFactory $problemDetailsFactory,
        array $config
    ) {
        $this->userService = $userService;
        $this->problemDetailsFactory = $problemDetailsFactory;
        $this->config = $config;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $post = $request->getParsedBody();

        /** @var User $user */
        $user = $this->userService->getOneBy(['email' => $post['email']]);

        if ($user && isset($post['password']) && password_verify($post['password'], $user->getPassword())) {

            $headerEncoded = base64_encode(json_encode($this->config['jwt']['header']));
            $payloadEncoded = base64_encode(json_encode(['id' => $user->getId()]));

            $jwt = $headerEncoded . '.' . $payloadEncoded . '.'
                . hash_hmac('sha256', $headerEncoded . '.' . $payloadEncoded, $this->config['jwt']['secret']);

            return new JsonResponse(['token' => $jwt]);
        }

        return $this->problemDetailsFactory->createResponse(
            $request,
            400,
            'Invalid login credentials.'
        );
    }
}