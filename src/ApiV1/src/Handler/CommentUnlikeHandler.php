<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\EntityNotFoundException;
use App\Domain\Entity\Comment;
use App\Domain\Entity\User;
use App\Service\CommentServiceInterface;
use App\Service\UserServiceInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Authentication\UserInterface;
use ApiV1\Handler\Exception;

class CommentUnlikeHandler implements RequestHandlerInterface
{
    private $userService;

    private $commentService;

    public function __construct(
        UserServiceInterface $userService,
        CommentServiceInterface $commentService
    ){
        $this->userService = $userService;
        $this->commentService = $commentService;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var Comment $comment */
        $comment = $this->commentService->getById($request->getAttribute('id'));

        if (!$comment) {
            throw new EntityNotFoundException();
        }

        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        $user->unlikeComment($comment);
        $comment->setLikesCount($comment->getLikesCount() - 1);

        $this->userService->persist($user);

        return (new Response())
            ->withHeader('Content-Type','application/json')
            ->withStatus(204);
    }
}