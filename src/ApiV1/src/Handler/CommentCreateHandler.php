<?php

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\ValidationFailedException;
use App\Domain\Entity\Comment;
use App\Domain\Entity\Proposal;
use App\InputFilter\CommentInputFilter;
use App\Service\CommentServiceInterface;
use App\Service\ProposalServiceInterface;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Authentication\UserInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class CommentCreateHandler implements RequestHandlerInterface
{
    private $userService;

    private $commentService;

    private $proposalService;

    private $inputFilter;

    private $resourceGenerator;

    private $responseFactory;

    public function __construct(
        UserServiceInterface $userService,
        CommentServiceInterface $commentService,
        ProposalServiceInterface $proposalService,
        CommentInputFilter $inputFilter,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory
    ){
        $this->userService = $userService;
        $this->commentService = $commentService;
        $this->proposalService = $proposalService;
        $this->inputFilter = $inputFilter;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $post = $request->getParsedBody();

        $user = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        $this->inputFilter->get('comment_id')->setRequired(!isset($post['proposal_id']) && !isset($post['comment_id']));
        $this->inputFilter->get('proposal_id')->setRequired(!isset($post['proposal_id']) && !isset($post['comment_id']));

        $this->inputFilter->setData($post);

        if (!$this->inputFilter->isValid()) {
            throw new ValidationFailedException($this->inputFilter->getMessages());
        }

        $comment = new Comment();
        $comment->setAuthor($user);

        if (isset($post['comment_id'])) {
            /** @var Comment $parentComment */
            $parentComment = $this->commentService->getById($post['comment_id']);

            $comment->setParent($parentComment);
            $comment->setProposal($parentComment->getProposal());

            $parentComment->setRepliesCount($parentComment->getRepliesCount() + 1);
        }

        if (isset($post['proposal_id']) && !isset($post['comment_id'])) {
            /** @var Proposal $proposal */
            $proposal = $this->proposalService->getById($post['proposal_id']);

            $comment->setProposal($proposal);
            $proposal->setCommentsCount($proposal->getCommentsCount() + 1);

            $this->proposalService->persist($proposal);
        }

        $comment->setText($this->inputFilter->getValue('text'));

        $this->commentService->persist($comment);

        $resource = $this->resourceGenerator->fromObject($comment, $request);

        return $this->responseFactory->createResponse($request, $resource)->withStatus(201);
    }
}