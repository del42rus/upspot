<?php

declare(strict_types=1);

namespace ApiV1\Handler;

use ApiV1\Handler\Exception\EntityNotFoundException;
use App\Domain\Entity\Proposal;
use App\Domain\Entity\User;
use App\Service\ProposalServiceInterface;
use App\Service\UserServiceInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Expressive\Authentication\UserInterface;
use ApiV1\Handler\Exception;
use Zend\ProblemDetails\ProblemDetailsResponseFactory;

class ProposalLikeHandler implements RequestHandlerInterface
{
    private $userService;

    private $proposalService;

    private $problemDetailsFactory;

    public function __construct(
        UserServiceInterface $userService,
        ProposalServiceInterface $proposalService,
        ProblemDetailsResponseFactory $problemDetailsFactory
    ){
        $this->userService = $userService;
        $this->proposalService = $proposalService;
        $this->problemDetailsFactory = $problemDetailsFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var Proposal $proposal */
        $proposal = $this->proposalService->getById($request->getAttribute('id'));

        if (!$proposal) {
            throw new EntityNotFoundException();
        }

        /** @var User $user */
        $user = $this->userService->getById($request->getAttribute(UserInterface::class)->getIdentity());

        $user->likeProposal($proposal);

        try {
            $this->userService->persist($user);
        } catch (UniqueConstraintViolationException $exception) {

        }

        return (new Response())
            ->withHeader('Content-Type','application/json')
            ->withStatus(204);
    }
}