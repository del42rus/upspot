<?php

namespace ApiV1\Handler;

use App\Domain\Collection\ParticipantPaginator;
use App\Service\UserServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Hal\HalResponseFactory;
use Zend\Expressive\Hal\ResourceGenerator;

class ParticipantCollectionShowHandler implements RequestHandlerInterface
{
    private $userService;

    private $resourceGenerator;

    private $responseFactory;

    private $config;

    public function __construct(
        UserServiceInterface $userService,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $responseFactory,
        $config
    ){
        $this->userService = $userService;
        $this->resourceGenerator = $resourceGenerator;
        $this->responseFactory = $responseFactory;
        $this->config = $config;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $metadataMap = $this->resourceGenerator->getMetadataMap();

        $metadata    = $metadataMap->get(ParticipantPaginator::class);
        $metadataQuery = $metadata->getQueryStringArguments();

        $queryParams = $request->getQueryParams();

        $page = $queryParams['page'] ?? 1;

        $itemCountPerPage = isset($queryParams['per_page']) && $queryParams['per_page'] <= $this->config['rest_api_max_page_size']
            ? $queryParams['per_page']
            : $this->config['rest_api_default_page_size'];

        $proposalId = $request->getAttribute('proposalId') ?: ($queryParams['proposal_id'] ?? null);
        $timeId = $request->getAttribute('timeId') ?: ($queryParams['time_id'] ?? null);

        if ($proposalId) {
            $collection = $this->userService->getParticipantsByProposalId($proposalId, $page, $itemCountPerPage);
            $metadataQuery = array_merge($metadataQuery, ['proposal_id' => $proposalId]);
        } elseif ($timeId) {
            $collection = $this->userService->getParticipantsByTimeIds(is_array($timeId) ? $timeId : [$timeId], $page, $itemCountPerPage);
            $metadataQuery = array_merge($metadataQuery, ['time_id' => $timeId]);
        } else {
            throw new \RuntimeException('No proposal identifier or time identifier(s) provided', 400);
        }

        if (isset($queryParams['per_page'])) {
            $metadataQuery = array_merge($metadataQuery, ['per_page' => $queryParams['per_page']]);
        }

        $metadata->setQueryStringArguments($metadataQuery);

        $resource  = $this->resourceGenerator->fromObject($collection, $request);

        return $this->responseFactory->createResponse($request, $resource);
    }
}