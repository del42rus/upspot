<?php

namespace App\Authentication\Factory;

use App\Authentication\JWTAuthentication;
use App\Domain\Repository\UserRepositoryInterface;
use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Expressive\Authentication\UserInterface;

class JWTAuthenticationFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UserRepositoryInterface $userRepository */
        $userRepository = $container->get(UserRepositoryInterface::class);

        /** @var callable $responseFactory */
        $responseFactory = $container->get(ResponseInterface::class);

        /** @var callable $userFactory */
        $userFactory = $container->get(UserInterface::class);

        return new JWTAuthentication(
            $userRepository,
            $responseFactory,
            $userFactory,
            $container->get('config')['authentication']
        );
    }
}