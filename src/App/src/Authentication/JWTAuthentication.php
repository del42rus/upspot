<?php

namespace App\Authentication;

use App\Domain\Entity\User;
use App\Domain\Repository\UserRepositoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Authentication\AuthenticationInterface;
use Zend\Expressive\Authentication\UserInterface;

class JWTAuthentication implements AuthenticationInterface
{
    private $userRepository;

    private $responseFactory;

    private $userFactory;

    private $config;

    public function __construct(
        UserRepositoryInterface $userRepository,
        callable $responseFactory,
        callable $userFactory,
        array $config
    ){
        $this->userRepository = $userRepository;

        $this->responseFactory = function () use ($responseFactory) : ResponseInterface {
            return $responseFactory();
        };

        $this->userFactory = function (
            string $identity,
            array $roles = [],
            array $details = []
        ) use ($userFactory) : UserInterface {
            return $userFactory($identity, $roles, $details);
        };

        $this->config = $config;
    }

    public function authenticate(ServerRequestInterface $request): ?UserInterface
    {
        $authHeaders = $request->getHeader('Authorization');

        if (1 !== count($authHeaders)) {
            return null;
        }

        $authHeader = array_shift($authHeaders);

        if (!preg_match('/Bearer (?P<jwt>(?P<headerEncoded>.+)\.(?<payloadEncoded>.+)\.(?<hash>.+))/', $authHeader, $match)) {
            return null;
        }

        $signature = hash_hmac('sha256', $match['headerEncoded'] . '.' . $match['payloadEncoded'], $this->config['jwt']['secret']);

        if ($match['hash'] != $signature) {
            return null;
        }

        $payload = (array) json_decode(base64_decode($match['payloadEncoded']));

        /** @var User $identity */
        $identity = $this->userRepository->getById($payload['id']);

        if (!$identity) {
            return null;
        }

        return ($this->userFactory)($identity->getId());
    }

    public function unauthorizedResponse(ServerRequestInterface $request): ResponseInterface
    {
        return ($this->responseFactory)()
            ->withStatus(401);
    }
}