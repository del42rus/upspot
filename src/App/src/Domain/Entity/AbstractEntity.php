<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Zend\Hydrator\ClassMethodsHydrator;
use Zend\Hydrator\Filter\FilterComposite;
use Zend\Hydrator\Filter\FilterProviderInterface;
use Zend\Hydrator\Filter\GetFilter;
use Zend\Hydrator\Filter\MethodMatchFilter;
use Zend\Hydrator\Filter\FilterInterface;

abstract class AbstractEntity implements FilterProviderInterface
{
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getFilter() : FilterInterface
    {
        $composite = new FilterComposite([], [new GetFilter(), new MethodMatchFilter('getArrayCopy')]);

        return $composite;
    }

    public function getArrayCopy()
    {
        return (new ClassMethodsHydrator())
            ->extract($this);
    }

    public function populate(array $data)
    {
        foreach ($data as $field => $value) {
            $setter = 'set' . ucfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $field))));
            $this->$setter($value);
        }

        return $this;
    }
}