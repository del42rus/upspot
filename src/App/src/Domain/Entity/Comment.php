<?php

namespace App\Domain\Entity;

use App\Domain\Collection\CommentCollection;
use App\Domain\Collection\CommentParentThreadCollection;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Hydrator\ClassMethodsHydrator;
use Zend\Hydrator\Filter\FilterComposite;
use Zend\Hydrator\Filter\FilterInterface;
use Zend\Hydrator\Filter\MethodMatchFilter;
use Zend\Hydrator\Strategy\DateTimeFormatterStrategy;

/**
 * Comment
 */
class Comment extends AbstractEntity
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \App\Domain\Entity\User
     */
    private $author;

    /**
     * @var int
     */
    private $repliesCount = 0;

    /**
     * @var int
     */
    private $likesCount = 0;

    /**
     * @var \App\Domain\Entity\Proposal
     */
    private $proposal;

    /**
     * @var Comment
     */
    private $parent;

    /**
     * @var ArrayCollection
     */
    private $replies;


    public function __construct()
    {
        $this->replies = new ArrayCollection();
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return Comment
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Comment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Comment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set author.
     *
     * @param \App\Domain\Entity\User $author
     *
     * @return Comment
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author.
     *
     * @return \App\Domain\Entity\User|null
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return int
     */
    public function getRepliesCount(): int
    {
        return $this->repliesCount;
    }

    /**
     * @param int $repliesCount
     * @return Comment
     */
    public function setRepliesCount(int $repliesCount): Comment
    {
        $this->repliesCount = $repliesCount;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasReplies()
    {
        return (bool) $this->replies->count();
    }

    /**
     * @return int
     */
    public function getLikesCount(): int
    {
        return $this->likesCount;
    }

    /**
     * @param int $likesCount
     * @return Comment
     */
    public function setLikesCount(int $likesCount): Comment
    {
        $this->likesCount = $likesCount >= 0 ? $likesCount : 0;

        return $this;
    }

    /**
     * Set proposal.
     *
     * @param \App\Domain\Entity\Proposal|null $proposal
     *
     * @return Comment
     */
    public function setProposal(Proposal $proposal)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * Get proposal.
     *
     * @return \App\Domain\Entity\Proposal|null
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * @return Comment
     */
    public function getParent(): ?Comment
    {
        return $this->parent;
    }

    /**
     * @param Comment $parent
     * @return Comment
     */
    public function setParent(?Comment $parent): Comment
    {
        $this->parent = $parent;

        return $this;
    }

    public function getParentThread()
    {
        $parent = $this->getParent();

        $thread = $parent ? [$parent] : [];

        if (null === $parent) {
            return new CommentParentThreadCollection($thread);
        } else {
            $thread = array_merge($thread, $parent->getParentThread()->toArray());
        }

        return new CommentParentThreadCollection(array_reverse($thread));
    }

    public function getArrayCopy()
    {
        $hydrator = new ClassMethodsHydrator();

        $hydrator->addStrategy('createdAt', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('updatedAt', new DateTimeFormatterStrategy('U'));

        return $hydrator->extract($this);
    }

    public function getFilter() : FilterInterface
    {
        /** @var FilterComposite $composite */
        $composite = parent::getFilter();

        $composite->addFilter('excludeParentThread', new MethodMatchFilter('getParentThread'), FilterComposite::CONDITION_AND);

        return $composite;
    }
}
