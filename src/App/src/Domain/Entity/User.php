<?php

namespace App\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Zend\Hydrator\ClassMethodsHydrator;
use Zend\Hydrator\Filter\FilterComposite;
use Zend\Hydrator\Filter\FilterInterface;
use Zend\Hydrator\Filter\MethodMatchFilter;
use Zend\Hydrator\Strategy\DateTimeFormatterStrategy;

/**
 * User
 */
class User extends AbstractEntity
{
    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var integer
     */
    private $gender;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var ArrayCollection
     */
    private $likedProposals;

    /**
     * @var ArrayCollection
     */
    private $likedComments;

    /**
     * @var ArrayCollection
     */
    private $followers;

    /**
     * @var ArrayCollection
     */
    private $followees;

    /**
     * @var ArrayCollection
     */
    private $blockedUsers;

    public function __construct()
    {
        $this->likedProposals = new ArrayCollection();
        $this->likedComments = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->followees = new ArrayCollection();
        $this->blockedUsers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): User
    {
        $this->username = $username;

        return $this;
    }
    /**
     * Set email.
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = password_hash($password, PASSWORD_BCRYPT);

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function getGender(): ?int
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     * @return User
     */
    public function setGender(int $gender): User
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMale(): bool
    {
        return (bool) $this->gender == 1;
    }

    /**
     * @return bool
     */
    public function isFemale(): bool
    {
        return (bool) $this->gender == 0;
    }

    /**
     * @return string
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return User
     */
    public function setAvatar(string $avatar): User
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikedProposals()
    {
        return $this->likedProposals;
    }

    public function likeProposal(Proposal $proposal)
    {
        $this->likedProposals[] = $proposal;
    }

    public function unlikeProposal(Proposal $proposal)
    {
        $this->likedProposals->removeElement($proposal);
    }

    public function getLikedComments()
    {
        return $this->likedComments;
    }

    public function likeComment(Comment $comment)
    {
        $this->likedComments[] = $comment;
    }

    public function unlikeComment(Comment $comment)
    {
        $this->likedComments->removeElement($comment);
    }

    public function addFollower(User $follower)
    {
        $this->followers[] = $follower;
        $follower->followees[] = $this;
    }

    public function removeFollower(User $follower)
    {
        $this->followers->removeElement($follower);
        $follower->followees->removeElement($this);
    }

    public function blockUser(User $user)
    {
        $this->blockedUsers[] = $user;
    }

    public function unblockUser(User $user)
    {
        $this->blockedUsers->removeElement($user);
    }

    public function getBlockedUsers()
    {
        return $this->blockedUsers;
    }

    public function getFollowers()
    {
        return $this->followers;
    }

    public function getArrayCopy()
    {
        $hydrator = new ClassMethodsHydrator();

        $hydrator->addStrategy('createdAt', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('updatedAt', new DateTimeFormatterStrategy('U'));

        return $hydrator->extract($this);
    }

    public function getFilter() : FilterInterface
    {
        /** @var FilterComposite $composite */
        $composite = parent::getFilter();

        $composite->addFilter('excludePassword', new MethodMatchFilter('getPassword'), FilterComposite::CONDITION_AND);
        $composite->addFilter('excludeLikedProposals', new MethodMatchFilter('getLikedProposals'), FilterComposite::CONDITION_AND);
        $composite->addFilter('excludeLikedComments', new MethodMatchFilter('getLikedComments'), FilterComposite::CONDITION_AND);
        $composite->addFilter('excludeFollowers', new MethodMatchFilter('getFollowers'), FilterComposite::CONDITION_AND);
        $composite->addFilter('excludeBlockedUsers', new MethodMatchFilter('getBlockedUsers'), FilterComposite::CONDITION_AND);

        return $composite;
    }
}
