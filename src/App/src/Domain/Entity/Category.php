<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Zend\Hydrator\ClassMethodsHydrator;

/**
 * InterestCategory
 */
class Category extends AbstractEntity
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $slug;


    /**
     * @var ArrayCollection
     */
    private $children;

    /**
     * @var Category
     */
    private $parent;

    public function __construct() {
        $this->children = new ArrayCollection();
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Category
     */
    public function setSlug(string $slug): Category
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Category
     */
    public function getParent(): ?Category
    {
        return $this->parent;
    }

    /**
     * @param Category $parent
     * @return Category
     */
    public function setParent(?Category $parent): ?Category
    {
        $this->parent = $parent;

        return $this;
    }

    public function getParentId()
    {
        return $this->parent ? $this->parent->getId() : null;
    }

    public function getChildrenIds($recursive = true)
    {
        $ids = [];

        /** @var Category $child */
        foreach ($this->children as $child) {
            $ids[] = $child->getId();

            if (true === $recursive) {
                $ids = array_merge($ids, $child->getChildrenIds(true));
            }
        }

        return $ids;
    }

    public function getArrayCopy()
    {
        return (new ClassMethodsHydrator())->extract($this);
    }
}
