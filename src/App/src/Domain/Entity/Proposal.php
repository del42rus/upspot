<?php

namespace App\Domain\Entity;

use App\Domain\Collection\AssistantCollection;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Hydrator\ClassMethodsHydrator;
use Zend\Hydrator\Strategy\DateTimeFormatterStrategy;

/**
 * Proposal
 */
class Proposal extends AbstractEntity
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $shortDescription;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string|null
     */
    private $location;

    /**
     * @var string|null
     */
    private $cost;

    /**
     * @var int|null
     */
    private $assistantsCount;

    /**
     * @var int|null
     */
    private $maxPeopleCount;

    /**
     * @var int|null
     */
    private $maxGirlsCount;

    /**
     * @var int|null
     */
    private $maxBoysCount;

    /**
     * @var int
     */
    private $commentsCount = 0;

    /**
     * @var \DateTime
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     */
    private $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \App\Domain\Entity\User
     */
    private $user;

    /**
     * @var ArrayCollection
     */
    private $categories;

    /**
     * @var AssistantCollection
     */
    private $assistants;

    /**
     * @var ArrayCollection
     */
    private $likers;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->assistants = new AssistantCollection();
        $this->likers = new ArrayCollection();
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Proposal
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set shortDescription.
     *
     * @param string $shortDescription
     *
     * @return Proposal
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription.
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Proposal
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set location.
     *
     * @param string|null $location
     *
     * @return Proposal
     */
    public function setLocation($location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location.
     *
     * @return string|null
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set cost.
     *
     * @param string|null $cost
     *
     * @return Proposal
     */
    public function setCost($cost = null)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost.
     *
     * @return string|null
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return int|null
     */
    public function getAssistantsCount(): ?int
    {
        return $this->assistantsCount;
    }

    /**
     * @param int|null $assistantsCount
     */
    public function setAssistantsCount(int $assistantsCount): void
    {
        $this->assistantsCount = $assistantsCount;
    }

    /**
     * @return int|null
     */
    public function getMaxPeopleCount(): ?int
    {
        return $this->maxPeopleCount;
    }

    /**
     * @param int|null $maxPeopleCount
     * @return Proposal
     */
    public function setMaxPeopleCount(?int $maxPeopleCount): Proposal
    {
        $this->maxPeopleCount = $maxPeopleCount;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxGirlsCount(): ?int
    {
        return $this->maxGirlsCount;
    }

    /**
     * @param int|null $maxGirlsCount
     * @return Proposal
     */
    public function setMaxGirlsCount(?int $maxGirlsCount): Proposal
    {
        $this->maxGirlsCount = $maxGirlsCount;

        $this->maxPeopleCount = $this->maxGirlsCount + $this->maxBoysCount;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxBoysCount(): ?int
    {
        return $this->maxBoysCount;
    }

    /**
     * @param int|null $maxBoysCount
     * @return Proposal
     */
    public function setMaxBoysCount(?int $maxBoysCount): Proposal
    {
        $this->maxBoysCount = $maxBoysCount;

        $this->maxPeopleCount = $this->maxGirlsCount + $this->maxBoysCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getCommentsCount(): int
    {
        return $this->commentsCount;
    }

    /**
     * @param int $commentsCount
     * @return Proposal
     */
    public function setCommentsCount(int $commentsCount): Proposal
    {
        $this->commentsCount = $commentsCount;

        return $this;
    }


    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user.
     *
     * @param \App\Domain\Entity\User|null $user
     *
     * @return Proposal
     */
    public function setUser(\App\Domain\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \App\Domain\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return AssistantCollection
     */
    public function getAssistants()
    {
        return new AssistantCollection($this->assistants->toArray());
    }

    public function getArrayCopy()
    {
        $hydrator = new ClassMethodsHydrator();

        $hydrator->addStrategy('createdAt', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('updatedAt', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('date', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('dateStart', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('dateStart', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('dateEnd', new DateTimeFormatterStrategy('U'));

        return $hydrator->extract($this);
    }
}
