<?php

namespace App\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Zend\Hydrator\ClassMethodsHydrator;
use Zend\Hydrator\Strategy\DateTimeFormatterStrategy;

/**
 * Time
 */
class Time extends AbstractEntity
{
    /**
     * @var \DateTime|null
     */
    private $date;

    /**
     * @var \DateTime|null
     */
    private $dateStart;

    /**
     * @var \DateTime|null
     */
    private $dateEnd;

    /**
     * @var \DateTime|null
     */
    private $time;

    /**
     * @var \DateTime|null
     */
    private $timeStart;

    /**
     * @var \DateTime|null
     */
    private $timeEnd;

    /**
     * @var int
     */
    private $peopleCount = '0';

    /**
     * @var int
     */
    private $girlsCount = '0';

    /**
     * @var int
     */
    private $boysCount = '0';

    /**
     * @var \DateTime
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     */
    private $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \App\Domain\Entity\Proposal
     */
    private $proposal;

    /**
     * @var ArrayCollection
     */
    private $participants;

    public function __construct()
    {
        $this->participants = new ArrayCollection();
    }

    /**
     * Set date.
     *
     * @param \DateTime|null $date
     *
     * @return Time
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set dateStart.
     *
     * @param \DateTime|null $dateStart
     *
     * @return Time
     */
    public function setDateStart($dateStart = null)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart.
     *
     * @return \DateTime|null
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd.
     *
     * @param \DateTime|null $dateEnd
     *
     * @return Time
     */
    public function setDateEnd($dateEnd = null)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd.
     *
     * @return \DateTime|null
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set time.
     *
     * @param \DateTime|null $time
     *
     * @return Time
     */
    public function setTime($time = null)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time.
     *
     * @return \DateTime|null
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set timeStart.
     *
     * @param \DateTime|null $timeStart
     *
     * @return Time
     */
    public function setTimeStart($timeStart = null)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart.
     *
     * @return \DateTime|null
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeEnd.
     *
     * @param \DateTime|null $timeEnd
     *
     * @return Time
     */
    public function setTimeEnd($timeEnd = null)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * Get timeEnd.
     *
     * @return \DateTime|null
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * Set peopleCount.
     *
     * @param int $peopleCount
     *
     * @return Time
     */
    public function setPeopleCount($peopleCount)
    {
        $this->peopleCount = $peopleCount;

        return $this;
    }

    /**
     * Get peopleCount.
     *
     * @return int
     */
    public function getPeopleCount()
    {
        return $this->peopleCount;
    }

    /**
     * Set girlsCount.
     *
     * @param int $girlsCount
     *
     * @return Time
     */
    public function setGirlsCount($girlsCount)
    {
        $this->girlsCount = $girlsCount > 0 ? $girlsCount : 0;

        $this->peopleCount = $this->girlsCount + $this->boysCount;

        return $this;
    }

    /**
     * Get girlsCount.
     *
     * @return int
     */
    public function getGirlsCount()
    {
        return $this->girlsCount;
    }

    /**
     * Set boysCount.
     *
     * @param int $boysCount
     *
     * @return Time
     */
    public function setBoysCount($boysCount)
    {
        $this->boysCount = $boysCount > 0 ? $boysCount : 0;

        $this->peopleCount = $this->girlsCount + $this->boysCount;

        return $this;
    }

    /**
     * Get boysCount.
     *
     * @return int
     */
    public function getBoysCount()
    {
        return $this->boysCount;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set proposal.
     *
     * @param \App\Domain\Entity\Proposal|null $proposal
     *
     * @return Time
     */
    public function setProposal(\App\Domain\Entity\Proposal $proposal = null)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * Get proposal.
     *
     * @return \App\Domain\Entity\Proposal|null
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * @param User $participant
     * @return $this
     */
    public function addParticipant(User $participant)
    {
        $this->participants[] = $participant;

        return $this;
    }

    /**
     * @param User $participant
     * @return $this
     */
    public function removeParticipant(User $participant)
    {
        $this->participants->removeElement($participant);

        return $this;
    }

    public function getArrayCopy()
    {
        $hydrator = new ClassMethodsHydrator();

        $hydrator->addStrategy('createdAt', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('updatedAt', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('date', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('date', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('dateStart', new DateTimeFormatterStrategy('U'));
        $hydrator->addStrategy('dateEnd', new DateTimeFormatterStrategy('U'));

        return $hydrator->extract($this);
    }
}
