<?php

namespace App\Domain\DTO;

use Zend\Hydrator\NamingStrategy\UnderscoreNamingStrategy;
use Zend\Hydrator\ObjectPropertyHydrator;
use Zend\Hydrator\Strategy\ClosureStrategy;

class Participant
{
    public $id;

    public $name;

    public $avatar;

    public $joinDate;

    public $lastParticipationDate;

    public $timeId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Participant
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     * @return Participant
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @param mixed $name
     * @return Participant
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastParticipationDate()
    {
        return $this->lastParticipationDate;
    }

    /**
     * @param mixed $lastParticipationDate
     * @return Participant
     */
    public function setLastParticipationDate($lastParticipationDate)
    {
        $this->lastParticipationDate = $lastParticipationDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJoinDate()
    {
        return $this->joinDate;
    }

    /**
     * @param mixed $joinDate
     */
    public function setJoinDate($joinDate): void
    {
        $this->joinDate = $joinDate;
    }

    /**
     * @return mixed
     */
    public function getTimeId()
    {
        return $this->timeId;
    }

    /**
     * @param mixed $timeId
     */
    public function setTimeId($timeId): void
    {
        $this->timeId = (int) $timeId;
    }

    public function getArrayCopy()
    {
        $hydrator = new ObjectPropertyHydrator();

        $hydrator->addStrategy('id', new ClosureStrategy(function ($value) { return (int) $value; }));
        $hydrator->addStrategy('timeId', new ClosureStrategy(function ($value) { return (int) $value; }));
        $hydrator->setNamingStrategy(new UnderscoreNamingStrategy());

        return $hydrator->extract($this);
    }
}