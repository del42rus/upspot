<?php

namespace App\Persistence\Doctrine\Repository\Factory;

use App\Domain\Repository\CommentRepositoryInterface;
use App\Persistence\Doctrine\Repository\CommentRepository;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class CommentRepositoryFactory
{
    public function __invoke(ContainerInterface $container) : CommentRepositoryInterface
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new CommentRepository($entityManager);
    }
}