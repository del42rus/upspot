<?php

declare(strict_types=1);

namespace App\Persistence\Doctrine\Repository\Factory;

use App\Persistence\Doctrine\Repository\ProposalRepository;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class ProposalRepositoryFactory
{
    public function __invoke(ContainerInterface $container) : ProposalRepository
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new ProposalRepository($entityManager);
    }
}
