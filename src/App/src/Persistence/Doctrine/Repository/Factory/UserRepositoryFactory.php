<?php

declare(strict_types=1);

namespace App\Persistence\Doctrine\Repository\Factory;

use App\Persistence\Doctrine\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class UserRepositoryFactory
{
    public function __invoke(ContainerInterface $container) : UserRepository
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new UserRepository($entityManager);
    }
}
