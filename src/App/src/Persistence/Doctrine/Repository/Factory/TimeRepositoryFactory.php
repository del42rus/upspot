<?php

declare(strict_types=1);

namespace App\Persistence\Doctrine\Repository\Factory;

use App\Persistence\Doctrine\Repository\TimeRepository;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class TimeRepositoryFactory
{
    public function __invoke(ContainerInterface $container) : TimeRepository
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new TimeRepository($entityManager);
    }
}
