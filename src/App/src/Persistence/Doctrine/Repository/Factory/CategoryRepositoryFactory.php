<?php

declare(strict_types=1);

namespace App\Persistence\Doctrine\Repository\Factory;

use App\Persistence\Doctrine\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class CategoryRepositoryFactory
{
    public function __invoke(ContainerInterface $container) : CategoryRepository
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new CategoryRepository($entityManager);
    }
}
