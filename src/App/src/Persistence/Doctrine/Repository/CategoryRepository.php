<?php

declare(strict_types=1);

namespace App\Persistence\Doctrine\Repository;

use App\Domain\Entity\Category;
use App\Domain\Repository\CategoryRepositoryInterface;

class CategoryRepository extends AbstractRepository implements CategoryRepositoryInterface
{
    protected $entityClass = Category::class;
}