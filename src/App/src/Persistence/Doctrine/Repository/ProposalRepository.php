<?php

namespace App\Persistence\Doctrine\Repository;

use App\Domain\Collection\ProposalPaginator;
use App\Domain\Entity\Proposal;
use App\Domain\Repository\ProposalRepositoryInterface;
use App\Paginator\Adapter\DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

class ProposalRepository extends AbstractRepository implements ProposalRepositoryInterface
{
    protected $entityClass = Proposal::class;

    public function getProposals(array $params, array $orderBy, $page, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('p')
            ->from($this->entityClass, 'p');

        foreach ($params as $key => $value) {
            if (empty($value)) {
                continue;
            }

            switch ($key) {
                case 'userId':
                    $queryBuilder->andWhere($queryBuilder->expr()->eq('p.user', $value));
                    break;
                case 'categoryId':
                    $queryBuilder->innerJoin('p.categories', 'c')
                        ->where($queryBuilder->expr()->eq('c.id', $value));
                    break;
                case 'categoryIds':
                    $queryBuilder->innerJoin('p.categories', 'c')
                        ->where($queryBuilder->expr()->in('c.id', $value));
                    break;
            }
        }

        if (!empty($orderBy)) {
            foreach ($orderBy as $sortField => $sortOrder) {
                $sortField = lcfirst(str_replace(' ', '',ucwords(str_replace('_', ' ', $sortField))));
                $queryBuilder->addOrderBy('p.' . $sortField, $sortOrder);
            }
        }

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new ProposalPaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

    public function getLikedProposalsByUserId($userId, array $orderBy, $page, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('p')
            ->from($this->entityClass, 'p')
            ->where(':userId MEMBER OF p.likers')
            ->setParameter('userId', $userId);

        if (!empty($orderBy)) {
            foreach ($orderBy as $sortField => $sortOrder) {
                $sortField = lcfirst(str_replace(' ', '',ucwords(str_replace('_', ' ', $sortField))));
                $queryBuilder->addOrderBy('p.' . $sortField, $sortOrder);
            }
        }

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new ProposalPaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }
}