<?php

namespace App\Persistence\Doctrine\Repository;

use App\Domain\Collection\TimePaginator;
use App\Domain\Entity\Time;
use App\Domain\Repository\TimeRepositoryInterface;
use App\Paginator\Adapter\DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

class TimeRepository extends AbstractRepository implements TimeRepositoryInterface
{
    protected $entityClass = Time::class;

    public function getTimetableByProposalId($proposalId, $page, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('t', 'COALESCE(t.date, t.dateStart) as HIDDEN sortDate')
            ->from($this->entityClass, 't')
            ->where('t.proposal = ?1')
            ->orderBy('sortDate', 'asc')
            ->setParameter(1, $proposalId);

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new TimePaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

    public function getTimetable($page, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('t')
            ->from($this->entityClass, 't');

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new TimePaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }
}