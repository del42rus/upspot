<?php

namespace App\Persistence\Doctrine\Repository;

use App\Domain\Collection\CommentPaginator;
use App\Domain\Entity\Comment;
use App\Domain\Repository\CommentRepositoryInterface;
use App\Paginator\Adapter\DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

class CommentRepository extends AbstractRepository implements CommentRepositoryInterface
{
    protected $entityClass = Comment::class;
    
    public function getComments($page, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('c')
            ->from($this->entityClass, 'c')
            ->orderBy('c.createdAt', 'desc');

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new CommentPaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

    public function getCommentsByProposalId($proposalId, $page, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('c')
            ->from($this->entityClass, 'c')
            ->orderBy('c.createdAt', 'desc')
            ->where('c.proposal = ?1')
            ->andWhere($queryBuilder->expr()->isNull('c.parent'))
            ->setParameter(1, $proposalId);

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new CommentPaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

    public function getCommentsByUserId($userId, $page, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('c')
            ->from($this->entityClass, 'c')
            ->orderBy('c.createdAt', 'desc')
            ->where('c.author = ?1')
            ->setParameter(1, $userId);

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new CommentPaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

    public function getRepliesByCommentId($commentId, $page, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('c')
            ->from($this->entityClass, 'c')
            ->orderBy('c.createdAt', 'asc')
            ->where('c.parent = ?1')
            ->setParameter(1, $commentId);

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new CommentPaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }
}