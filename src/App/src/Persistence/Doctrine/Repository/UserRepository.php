<?php

namespace App\Persistence\Doctrine\Repository;

use App\Domain\Collection\FolloweePaginator;
use App\Domain\Collection\FollowerPaginator;
use App\Domain\Collection\ParticipantPaginator;
use App\Domain\Collection\UserPaginator;
use App\Domain\DTO\Participant;
use App\Domain\Entity\User;
use App\Domain\Repository\UserRepositoryInterface;
use App\Paginator\Adapter\DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Adapter\Callback;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    protected $entityClass = User::class;

    public function getUsers($page, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('u')
            ->from($this->entityClass, 'u')
            ->orderBy('u.createdAt', 'desc');

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new UserPaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

    public function getParticipantsByProposalId($proposalId, $page, $itemCountPerPage = 20)
    {
        $connection = $this->entityManager->getConnection();

        $queryBuilder = $connection->createQueryBuilder();

        $queryBuilder->select([
                'u.id', 'u.name', 'u.avatar',
                'MIN(UNIX_TIMESTAMP(utt.created_at)) as joinDate']
            )
            ->from('users', 'u')
            ->innerJoin('u', 'users_to_timetable', 'utt', 'u.id = utt.user_id')
            ->innerJoin('utt', 'timetable', 't', 't.id = utt.time_id')
            ->where('t.proposal_id = ?')
            ->groupBy('utt.user_id')
            ->setParameter(0, $proposalId);

        $count = $connection->createQueryBuilder()
            ->select('COUNT(*)')
            ->from('(' . $queryBuilder->getSQL() . ') AS t1')
            ->setParameter(0, $proposalId)
            ->execute()
            ->fetchColumn();

        $lastParticipationDates = $connection->createQueryBuilder()
            ->select('u.id', 'UNIX_TIMESTAMP(MAX(IF(t.date, t.date, t.date_start))) as last_participation_date')
            ->from('users', 'u')
            ->innerJoin('u', 'users_to_timetable', 'utt', 'u.id = utt.user_id')
            ->innerJoin('utt', 'timetable', 't', 'utt.time_id = t.id AND (t.date_start <= CURRENT_DATE() OR t.date <= CURRENT_DATE())')
            ->where('t.proposal_id = ?')
            ->groupBy('u.id')
            ->setParameter(0, $proposalId)
            ->execute()
            ->fetchAll(\PDO::FETCH_KEY_PAIR);

        $adapter = new Callback(function ($offset, $itemCountPerPage) use ($queryBuilder, $lastParticipationDates) {
            $queryBuilder->setFirstResult($offset)
                ->setMaxResults($itemCountPerPage);

            $stmt = $queryBuilder->execute();

            $result = $stmt->fetchAll(\PDO::FETCH_CLASS, Participant::class);

            /** @var Participant $participant */
            foreach ($result as $participant) {
                if (!in_array($participant->getId(), array_keys($lastParticipationDates))) {
                    continue;
                }

                $participant->setLastParticipationDate($lastParticipationDates[$participant->getId()]);
            }

            return $result;
        }, function () use ($count) {
            return $count;
        });

        $paginator = new ParticipantPaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

    public function getParticipantsByTimeIds($timeIds, $page, $itemCountPerPage)
    {
        $connection = $this->entityManager->getConnection();

        $queryBuilder = $connection->createQueryBuilder();

        $queryBuilder->select([
                'u.id', 'u.name', 'u.avatar', 'utt.time_id',
                'UNIX_TIMESTAMP(utt.created_at) as joinDate']
            )
            ->from('users', 'u')
            ->innerJoin('u', 'users_to_timetable', 'utt', 'u.id = utt.user_id')
            ->where('utt.time_id IN (?)')
            ->setParameter(0, $timeIds, \Doctrine\DBAL\Connection::PARAM_INT_ARRAY);

        $adapter = new Callback(function ($offset, $itemCountPerPage) use ($queryBuilder) {
            $queryBuilder->setFirstResult($offset)
                ->setMaxResults($itemCountPerPage);

            $stmt = $queryBuilder->execute();

            $result = $stmt->fetchAll(\PDO::FETCH_CLASS, Participant::class);

            return $result;
        }, function () use ($queryBuilder) {
            $queryBuilder = clone $queryBuilder;
            return $queryBuilder
                ->resetQueryPart('select')
                ->select('COUNT(*)')
                ->execute()
                ->fetchColumn();
        });

        $paginator = new ParticipantPaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

    public function getFollowersByUserId($userId, $page = null, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('u')
            ->from($this->entityClass, 'u')
            ->where(':userId MEMBER OF u.followees')
            ->setParameter('userId', $userId);

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new FollowerPaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

    public function getFolloweesByUserId($userId, $page = null, $itemCountPerPage = 20)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('u')
            ->from($this->entityClass, 'u')
            ->where(':userId MEMBER OF u.followers')
            ->setParameter('userId', $userId);

        $query = $queryBuilder->getQuery();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new FolloweePaginator($adapter);

        $paginator
            ->setCurrentPageNumber((int) $page)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }
}