<?php

namespace App\Service;

use App\Domain\Repository\CommentRepositoryInterface;

class CommentService implements CommentServiceInterface
{
    private $commentRepository;

    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function __call($method, $parameters) {
        return $this->commentRepository->$method(...$parameters);
    }
}