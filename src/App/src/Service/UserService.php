<?php

namespace App\Service;

use App\Domain\Repository\UserRepositoryInterface;

class UserService implements UserServiceInterface
{
    private $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ){
        $this->userRepository = $userRepository;
    }

    public function __call($method, $parameters) {
        return $this->userRepository->$method(...$parameters);
    }
}