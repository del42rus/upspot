<?php

namespace App\Service;

use App\Domain\Repository\ProposalRepositoryInterface;

class ProposalService implements ProposalServiceInterface
{
    private $proposalRepository;

    public function __construct(ProposalRepositoryInterface $proposalRepository)
    {
        $this->proposalRepository = $proposalRepository;
    }

    public function __call($method, $parameters) {
        return $this->proposalRepository->$method(...$parameters);
    }
}