<?php

declare(strict_types=1);

namespace App\Service;

use App\Domain\Repository\TimeRepositoryInterface;

class TimeService implements TimeServiceInterface
{
    private $timeRepository;

    public function __construct(TimeRepositoryInterface $timeRepository)
    {
        $this->timeRepository = $timeRepository;
    }

    public function __call($method, $parameters) {
        return $this->timeRepository->$method(...$parameters);
    }
}