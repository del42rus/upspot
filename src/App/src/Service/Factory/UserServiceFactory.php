<?php

namespace App\Service\Factory;

use App\Domain\Repository\UserRepositoryInterface;
use App\Service\UserService;
use Interop\Container\ContainerInterface;

class UserServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $userRepository = $container->get(UserRepositoryInterface::class);

        return new UserService(
            $userRepository
        );
    }
}