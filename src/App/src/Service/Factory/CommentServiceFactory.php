<?php

namespace App\Service\Factory;

use App\Domain\Repository\CommentRepositoryInterface;
use App\Service\CommentService;
use Interop\Container\ContainerInterface;

class CommentServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $commentRepository = $container->get(CommentRepositoryInterface::class);

        return new CommentService(
            $commentRepository
        );
    }
}