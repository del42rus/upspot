<?php

namespace App\Service\Factory;

use App\Domain\Repository\TimeRepositoryInterface;
use App\Service\TimeService;
use Interop\Container\ContainerInterface;

class TimeServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $timeRepository = $container->get(TimeRepositoryInterface::class);

        return new TimeService(
            $timeRepository
        );
    }
}