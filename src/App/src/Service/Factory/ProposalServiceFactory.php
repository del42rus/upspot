<?php

namespace App\Service\Factory;

use App\Domain\Repository\ProposalRepositoryInterface;
use App\Service\ProposalService;
use Interop\Container\ContainerInterface;

class ProposalServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $proposalRepository = $container->get(ProposalRepositoryInterface::class);

        return new ProposalService(
            $proposalRepository
        );
    }
}