<?php

namespace App\InputFilter;

use DoctrineModule\Validator\ObjectExists;
use Zend\Filter\StringTrim;
use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;
use Doctrine\Common\Persistence\ObjectRepository;

class CommentInputFilter extends InputFilter
{
    private $commentRepository;

    private $proposalRepository;

    public function __construct(
        ObjectRepository $commentRepository,
        ObjectRepository $proposalRepository
    ){
        $this->commentRepository = $commentRepository;
        $this->proposalRepository = $proposalRepository;
    }

    public function init()
    {
        $this->add([
            'name' => 'text',
            'required' => true,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 1,
                        'max' => 140,
                    ],
                ],
            ],
            'filters' => [
                [
                    'name' => StringTrim::class
                ]
            ]
        ]);

        $this->add([
            'name' => 'comment_id',
            'required' => false,
            'filters'  => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                (new NotEmpty())->setMessage("Value is required and can't be empty if 'proposal_id' is not specified", NotEmpty::IS_EMPTY),
                [
                    'name' => ObjectExists::class,
                    'options' => [
                        'fields' => 'id',
                        'object_repository' => $this->commentRepository,
                    ]
                ],
            ],
        ]);

        $this->add([
            'name' => 'proposal_id',
            'required' => false,
            'filters'  => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                (new NotEmpty())->setMessage("Value is required and can't be empty if 'comment_id' is not specified", NotEmpty::IS_EMPTY),
                [
                    'name' => ObjectExists::class,
                    'options' => [
                        'fields' => 'id',
                        'object_repository' => $this->proposalRepository,
                    ]
                ],
            ],
        ]);
    }
}