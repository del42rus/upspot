<?php

namespace App\InputFilter;

use App\Domain\Entity\User;
use Doctrine\Common\Persistence\ObjectRepository;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Callback;
use Zend\Validator\Identical;
use Zend\Validator\StringLength;

class UserInputFilter extends InputFilter
{
    private $userRepository;

    public function __construct(ObjectRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function init()
    {
        $this->add([
            'name' => 'name',
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 50,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'username',
            'validators' => [
                (new Callback([$this, 'validateUsername']))->setMessage('A user with this username already exist.', Callback::INVALID_VALUE),
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 30,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'email',
            'filters'  => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                (new Callback([$this, 'validateEmail']))->setMessage('A user with this username already exist.', Callback::INVALID_VALUE),
            ],
        ]);

        $this->add([
            'required' => false,
            'name' => 'password',
            'filters'  => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 6,
                        'max' => 20,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'gender',
        ]);

        $this->add([
            'required' => false,
            'name' => 'password',
            'filters'  => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 6,
                        'max' => 20,
                    ],
                ],
            ],
        ]);

        $this->add([
            'required' => false,
            'name' => 'confirm',
            'filters'  => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => Identical::class,
                    'options' => [
                        'token' => 'password',
                        'messages' => [
                            'notSame' => 'Value doesn\'t match!',
                            'missingToken' => 'Value doesn\'t match!'
                        ]
                    ],
                ],
            ],
        ]);
    }

    public function validateUsername($value, $context)
    {
        /** @var User $user */
        $user = $this->userRepository->findOneByUsername($value);

        if (($user && !isset($context['id'])) ||
            ($user && $user->getId() != $context['id'])) {
            return false;
        }

        return true;
    }

    public function validateEmail($value, $context)
    {
        /** @var User $user */
        $user = $this->userRepository->findOneByEmail($value);

        if (($user && !isset($context['id'])) ||
            ($user && $user->getId() != $context['id'])) {
            return false;
        }

        return true;
    }
}