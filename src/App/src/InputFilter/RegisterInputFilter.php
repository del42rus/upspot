<?php

namespace App\InputFilter;

use Doctrine\Common\Persistence\ObjectRepository;
use DoctrineModule\Validator\NoObjectExists;
use Zend\Filter\StringTrim;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;

class RegisterInputFilter extends InputFilter
{
    private $userRepository;

    public function __construct(ObjectRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function init()
    {
        $this->add([
            'name' => 'name',
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 50,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'username',
            'validators' => [
                [
                    'name' => NoObjectExists::class,
                    'options' => [
                        'fields' => 'username',
                        'object_repository' => $this->userRepository,
                        'messages' => [
                            'objectFound' => 'A user with this username already exist.',
                        ]
                    ]
                ],
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 30,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'email',
            'filters'  => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => NoObjectExists::class,
                    'options' => [
                        'fields' => 'email',
                        'object_repository' => $this->userRepository,
                        'messages' => [
                            'objectFound' => 'A user with this email already exist.',
                        ]
                    ]
                ],
            ],
        ]);

        $this->add([
            'name' => 'password',
            'filters'  => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 6,
                        'max' => 20,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'agreed_with_terms',
            'required' => true,
        ]);
    }
}