<?php

declare(strict_types=1);

namespace App\InputFilter;

use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Identical;
use Zend\Validator\StringLength;

class PasswordChangeInputFilter extends InputFilter
{
    public function init()
    {
        $this->add([
            'name' => 'old_password',
            'filters'  => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 6,
                        'max' => 20,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'new_password',
            'filters'  => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 6,
                        'max' => 20,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'confirm_password',
            'filters'  => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => Identical::class,
                    'options' => [
                        'token' => 'new_password',
                        'messages' => [
                            'notSame' => 'Value doesn\'t match!',
                            'missingToken' => 'Value doesn\'t match!'
                        ]
                    ],
                ],
            ],
        ]);
    }
}