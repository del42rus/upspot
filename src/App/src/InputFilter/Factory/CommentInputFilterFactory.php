<?php

declare(strict_types=1);

namespace App\InputFilter\Factory;

use App\Domain\Entity\Comment;
use App\Domain\Entity\Proposal;
use App\InputFilter\CommentInputFilter;
use Doctrine\Common\Persistence\ObjectRepository;
use Interop\Container\ContainerInterface;

class CommentInputFilterFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var ObjectRepository $repository */
        $commentRepository = $container->get('doctrine.entitymanager.orm_default')->getRepository(Comment::class);

        /** @var ObjectRepository $repository */
        $proposalRepository = $container->get('doctrine.entitymanager.orm_default')->getRepository(Proposal::class);

        return new CommentInputFilter(
            $commentRepository,
            $proposalRepository
        );
    }
}