<?php

declare(strict_types=1);

namespace App\InputFilter\Factory;

use App\Domain\Entity\User;
use App\InputFilter\UserInputFilter;
use Doctrine\Common\Persistence\ObjectRepository;
use Interop\Container\ContainerInterface;

class UserInputFilterFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var ObjectRepository $repository */
        $userRepository = $container->get('doctrine.entitymanager.orm_default')->getRepository(User::class);

        return new UserInputFilter($userRepository);
    }
}