<?php

declare(strict_types=1);

namespace App;

use App\Authentication\Factory\JWTAuthenticationFactory;
use App\Authentication\JWTAuthentication;
use App\Domain\Repository\TimeRepositoryInterface;
use App\Domain\Repository\UserRepositoryInterface;
use App\Domain\Repository\CategoryRepositoryInterface;
use App\Domain\Repository\CommentRepositoryInterface;
use App\Domain\Repository\ProposalRepositoryInterface;
use App\InputFilter;
use App\Persistence\Doctrine\Repository\Factory\CategoryRepositoryFactory;
use App\Persistence\Doctrine\Repository\Factory\CommentRepositoryFactory;
use App\Persistence\Doctrine\Repository\Factory\ProposalRepositoryFactory;
use App\Persistence\Doctrine\Repository\Factory\TimeRepositoryFactory;
use App\Persistence\Doctrine\Repository\Factory\UserRepositoryFactory;
use App\Service\CommentServiceInterface;
use App\Service\Factory\CommentServiceFactory;
use App\Service\Factory\ProposalServiceFactory;
use App\Service\Factory\TimeServiceFactory;
use App\Service\Factory\UserServiceFactory;
use App\Service\ProposalServiceInterface;
use App\Service\TimeServiceInterface;
use App\Service\UserServiceInterface;
use Gedmo\Timestampable\TimestampableListener;
use Zend\Expressive\Authentication;
use Zend\ServiceManager\Factory\InvokableFactory;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => [
                'invokables' => [

                ],
                'factories'  => [
                    CategoryRepositoryInterface::class => CategoryRepositoryFactory::class,
                    UserRepositoryInterface::class => UserRepositoryFactory::class,
                    ProposalRepositoryInterface::class => ProposalRepositoryFactory::class,
                    CommentRepositoryInterface::class => CommentRepositoryFactory::class,
                    TimeRepositoryInterface::class => TimeRepositoryFactory::class,
                    UserServiceInterface::class => UserServiceFactory::class,
                    CommentServiceInterface::class => CommentServiceFactory::class,
                    ProposalServiceInterface::class => ProposalServiceFactory::class,
                    TimeServiceInterface::class => TimeServiceFactory::class,
                    JWTAuthentication::class => JWTAuthenticationFactory::class,
                ],
                'aliases' => [
                    Authentication\AuthenticationInterface::class => JWTAuthentication::class,
                ]
            ],
            'input_filters' => [
                'factories' => [
                    InputFilter\CommentInputFilter::class => InputFilter\Factory\CommentInputFilterFactory::class,
                    InputFilter\RegisterInputFilter::class => InputFilter\Factory\RegisterInputFilterFactory::class,
                    InputFilter\UserInputFilter::class => InputFilter\Factory\UserInputFilterFactory::class,
                    InputFilter\PasswordChangeInputFilter::class => InvokableFactory::class,
                ],
            ],
            'doctrine' => [
                'event_manager' => [
                    'orm_default' => [
                        'subscribers' => [
                            TimestampableListener::class
                        ],
                    ],
                ],
                'driver' => [
                    'orm_default' => [
                        'class' => \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain::class,
                        'drivers' => [
                            __NAMESPACE__ => __NAMESPACE__ . '_driver',
                        ],
                    ],
                    __NAMESPACE__ . '_driver' => [
                        'class' => \Doctrine\ORM\Mapping\Driver\YamlDriver::class,
                        'cache' => 'array',
                        'extension' => '.dcm.yml',
                        'paths' => [__DIR__ . '/../yml']
                    ],
                ],
            ],
        ];
    }
}
