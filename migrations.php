<?php
return [
    'name' => 'DoctrineMigrations',
    'migrations_namespace' => 'DoctrineMigrations',
    'table_name' => 'doctrine_migrations',
    'column_name' => 'version',
    'column_length' => 255,
    'executed_at_column_name' => 'executed_at',
    'migrations_directory' => '/data/migrations',
    'all_or_nothing' => true,
];