<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190220114101 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('RENAME TABLE `interest_categories` TO `categories`');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('RENAME TABLE `categories` TO `interest_categories`');
    }
}
