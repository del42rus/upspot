<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190307085131 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $sql =<<<SQL
    CREATE TABLE IF NOT EXISTS `blocked_users` (
      `id` INT (11) NOT NULL AUTO_INCREMENT,
      `blocked_user_id` INT (11) NOT NULL,
      `user_id` INT (11) NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE (`blocked_user_id`, `user_id`)
    )ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE IF NOT EXISTS `blocked_users`');
    }
}
