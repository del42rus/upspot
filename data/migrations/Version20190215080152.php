<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190215080152 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $sql =<<<SQL
    CREATE TABLE IF NOT EXISTS `timetable` (
      `id` INT (11) NOT NULL AUTO_INCREMENT,
      `proposal_id` INT (11) NOT NULL,
      `date` DATE DEFAULT NULL,
      `date_start` DATE DEFAULT NULL,
      `date_end` DATE DEFAULT NULL,
      `time` TIME DEFAULT NULL,
      `time_start` TIME DEFAULT NULL,
      `time_end` TIME DEFAULT NULL,
      `people_count` INT (11) NOT NULL DEFAULT 0,
      `girls_count` INT (11) NOT NULL DEFAULT 0,
      `boys_count` INT (11) NOT NULL DEFAULT 0,
      PRIMARY KEY (`id`)
    )ENGINE=InnoDb DEFAULT CHARSET=utf8;
SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE IF EXISTS `timetable`');
    }
}
