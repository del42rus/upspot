<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190215081305 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `proposals` DROP `people_count`');
        $this->addSql('ALTER TABLE `proposals` DROP `girls_count`');
        $this->addSql('ALTER TABLE `proposals` DROP `boys_count`');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `proposals` ADD COLUMN `people_count` INT (11) NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE `proposals` ADD COLUMN `girls_count` INT (11) NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE `proposals` ADD COLUMN `boys_count` INT (11) NOT NULL DEFAULT 0');
    }
}
