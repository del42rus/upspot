<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190213133913 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `proposals` ADD COLUMN `assistants_count` INT (11) NOT NULL DEFAULT 0 AFTER `cost`');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `proposals` DROP COLUMN `assistants_count`');
    }
}
