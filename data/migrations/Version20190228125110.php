<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190228125110 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `users` ADD COLUMN `name` VARCHAR (255) AFTER `id`');
        $this->addSql('UPDATE `users` SET `name` = CONCAT(`firstname`, " ", `lastname`)');
        $this->addSql('ALTER TABLE `users` DROP `firstname`');
        $this->addSql('ALTER TABLE `users` DROP `lastname`');
        $this->addSql('ALTER TABLE `users` CHANGE COLUMN `login` `username` VARCHAR (255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
