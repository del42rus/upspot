<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190220142311 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `comments` ADD COLUMN `parent_id` INT (11) AFTER `proposal_id`');
        $this->addSql('ALTER TABLE `comments` ADD COLUMN `replies_count` INT (11) NOT NULL DEFAULT 0 AFTER `text`');
        $this->addSql('ALTER TABLE `comments` MODIFY `proposal_id` INT (11) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
