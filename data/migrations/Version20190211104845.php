<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190211104845 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $sql =<<<SQL
    CREATE TABLE IF NOT EXISTS `proposals_to_categories` (
      `id` INT (11) NOT NULL AUTO_INCREMENT,
      `proposal_id` INT (11) NOT NULL,
      `category_id` INT (11) NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDb DEFAULT CHARSET=utf8;
SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE IF EXISTS `proposals_to_categories`');
    }
}
