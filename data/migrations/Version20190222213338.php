<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190222213338 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `users` DROP `api_token`');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `users` ADD COLUMN `api_token` VARCHAR (60) UNIQUE AFTER `password`');
    }
}
