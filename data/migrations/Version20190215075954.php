<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190215075954 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('DROP TABLE IF EXISTS `participants`');
    }

    public function down(Schema $schema) : void
    {
        $sql =<<<SQL
    CREATE TABLE IF NOT EXISTS `participants` (
      `id` INT (11) NOT NULL AUTO_INCREMENT,
      `user_id` INT (11) NOT NULL,
      `proposal_id` INT (11) NOT NULL,
      PRIMARY KEY (`id`)
    )ENGINE=InnoDb DEFAULT CHARSET=utf8;
SQL;

        $this->addSql($sql);
    }
}
