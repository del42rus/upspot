<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190210154349 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $sql =<<<SQL
    CREATE TABLE IF NOT EXISTS `proposals` (
      `id` INT (11) NOT NULL AUTO_INCREMENT,
      `user_id` INT (11) NOT NULL,
      `title` VARCHAR (255) NOT NULL,
      `short_description` VARCHAR (255) NOT NULL,
      `description` TEXT NOT NULL,
      `location` VARCHAR (255) DEFAULT NULL,
      `date_start` DATE NOT NULL,
      `date_end` DATE NOT NULL,
      `cost` DECIMAL (8, 2) DEFAULT NULL,
      `people_count` INT (11) DEFAULT NULL,
      `girls_count` INT (11) DEFAULT NULL,
      `boys_count` INT (11) DEFAULT NULL,
      `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDb DEFAULT CHARSET=utf8;
SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE IF EXISTS `proposals`');
    }
}
