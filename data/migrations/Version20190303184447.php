<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190303184447 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('RENAME TABLE `saved_proposals` TO `liked_proposals`');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('RENAME TABLE `liked_proposals` TO `saved_proposals`');
    }
}
