<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190228083739 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `comments` ADD COLUMN `likes_count` INT (11) NOT NULL DEFAULT 0 AFTER `replies_count`');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `comments` DROP `likes_count`');
    }
}
