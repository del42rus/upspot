<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190211160943 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `proposals` ADD COLUMN `date` DATE DEFAULT NULL AFTER `location`');
        $this->addSql('ALTER TABLE `users` ADD COLUMN `login` VARCHAR (255) NOT NULL AFTER `id`');
        $this->addSql('ALTER TABLE `users` ADD COLUMN `gender` TINYINT (1) DEFAULT NULL AFTER `lastname`');
    }

    public function down(Schema $schema) : void
    {

    }
}
