<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190213094931 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `proposals` ADD COLUMN `comments_count` INT (11) NOT NULL DEFAULT 0 AFTER `boys_count`');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE DROP `comments_count`');
    }
}
