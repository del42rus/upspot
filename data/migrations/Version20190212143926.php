<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190212143926 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `proposals` ADD COLUMN `max_people_count` INT (11) AFTER `cost`');
        $this->addSql('ALTER TABLE `proposals` ADD COLUMN `max_girls_count` INT (11) AFTER `max_people_count`');
        $this->addSql('ALTER TABLE `proposals` ADD COLUMN `max_boys_count` INT (11) AFTER `max_girls_count`');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
